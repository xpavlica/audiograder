﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace AudioGrader.Common.Models
{
    public class GraderResult
    {
        [JsonProperty]
        public float Score { get; }

        public GraderResult(float score)
        {
            Score = Math.Min(Constants.MaxMos, Math.Max(Constants.MinMos, score));
        }
    }
}
