﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace AudioGrader.Common.Models
{
    /// <summary>
    /// A single-channel discrete 1D signal.
    /// </summary>
    public class DiscreteSignalDto
{
        /// <summary>
        /// The signal samples
        /// </summary>
        [SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "DTOs can break this rule.")]
        public float[] Samples { get; }


        /// <summary>
        /// The sampling rate (in Hz) of the signal
        /// </summary>
        public int SampleRate { get; }

        /// <summary>
        /// Creates a new DTO.
        /// </summary>
        /// <param name="sampleRate">The sample rate</param>
        /// <param name="samples">The sample array</param>
        /// <param name="copy">Copies the <paramref name="samples"/> array if set to true. Otherwise the array is only referenced.</param>
        public DiscreteSignalDto(int sampleRate, [NotNull] float[] samples, bool copy = false)
        {
            SampleRate = sampleRate;
            if (!copy)
            {
                Samples = samples;
                return;
            }
            Samples = new float[samples.Length];
            Buffer.BlockCopy(samples, 0, Samples, 0, samples.Length * sizeof(float));
        }

        /// <summary>
        /// Creates a new DTO.
        /// </summary>
        /// <param name="sampleRate">The sample rate</param>
        /// <param name="samples">The sample collection</param>
        public DiscreteSignalDto(int sampleRate, [NotNull] IEnumerable<float> samples) : this(sampleRate, samples.ToArray(), false) { }
    }
}
