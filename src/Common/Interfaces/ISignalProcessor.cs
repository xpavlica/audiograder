﻿using AudioGrader.Common.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Threading.Tasks;

namespace AudioGrader.Common.Interfaces
{
    public interface ISignalProcessor
    {
        /// <summary>
        /// Runs the <paramref name="signal"/> through the processor.
        /// </summary>
        /// <remarks>The <paramref name="signal"/> is expected to not be modified by classes implementing this interface.</remarks>
        /// <param name="signal">The target signal</param>
        /// <returns>The transformed signal</returns>
        Task<DiscreteSignalDto?> Apply([NotNull] DiscreteSignalDto signalDto);
    }
}
