﻿using AudioGrader.Common.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace AudioGrader.Common.Interfaces
{
    public interface IAudioDecoder
    {
        /// <summary>
        /// Does this decoder support the given file format.
        /// </summary>
        /// <param name="stream">A Stream containing the audio data. <see cref="Stream.Position"/> is assumed to be set to 0.</param>
        /// <returns>True if the format is supported.</returns>
        Task<bool> Supports([NotNull] Stream stream);

        /// <summary>
        /// Converts the given <paramref name="stream"/> to a sequence of normalized discrete signals of each channel.
        /// </summary>
        /// <param name="stream">Input audio data. <see cref="Stream.Position"/> is assumed to be set to 0.</param>
        /// <returns>A sequence of <see cref="DiscreteSignalDto" /> objects if the conversion is successfull, <see cref="null"/> otherwise.</returns>
        Task<IEnumerable<DiscreteSignalDto>?> ToChannelSignals([NotNull] Stream stream);
    }
}
