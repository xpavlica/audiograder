﻿using AudioGrader.Common.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Threading.Tasks;

namespace AudioGrader.Common.Interfaces
{
    public interface IGrader
    {
        /// <summary>
        /// Asynchronously grades the input signal on a MOS scale.
        /// </summary>
        /// <param name="signals">A collection the signal's audio channels.</param>
        /// <returns>A <see cref="GraderResult"/> object if successful, null otherwise.</returns>
        Task<GraderResult?> Grade([NotNull] IEnumerable<DiscreteSignalDto> signals);
    }
}
