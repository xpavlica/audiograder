﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AudioGrader.Common.Interfaces
{
    public interface IModuleConfigurationProvider
    {
        /// <summary>
        /// Loads the configuration of the given type.
        /// </summary>
        /// <typeparam name="TConfigModel">The configuration data type.</typeparam>
        /// <returns>The loaded configuration or the default one if it does not exist.</returns>
        TConfigModel GetConfiguration<TConfigModel>() where TConfigModel : class, IModuleConfiguration<TConfigModel>, new();
    }
}
