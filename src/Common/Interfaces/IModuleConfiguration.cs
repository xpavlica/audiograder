﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace AudioGrader.Common.Interfaces
{
    /// <summary>
    /// An interface for module configuration models. An empty constructor should return the default configuration values.
    /// </summary>
    /// <typeparam name="TConfigModel"></typeparam>
    public interface IModuleConfiguration<TConfigModel> where TConfigModel : class, IModuleConfiguration<TConfigModel>, new()
    {
        /// <summary>
        /// Loads the given <typeparamref name="TConfigModel"/> from the <paramref name="uri"/>.
        /// </summary>
        /// <param name="uri">The <see cref="Uri"/> of the configuration resource.</param>
        /// <returns>The loaded configuration if successful, <see cref="null"/> otherwise.</returns>
        TConfigModel? Load([NotNull] Uri uri);
        /// <summary>
        /// Saves the config instance to the resource pointed by <paramref name="uri"/>.
        /// </summary>
        /// <param name="uri">The <see cref="Uri"/> of the configuration resource.</param>
        /// <returns>True if the configuration was successfully saved.</returns>
        bool Save([NotNull] Uri uri);
        /// <summary>
        /// The module's type
        /// </summary>
        Type ModuleType { get; }
        /// <summary>
        /// The preferred extension of this configuration file (without the leading dot). 
        /// Can be null if the configuration is not saved in a file or no extension is preferred.
        /// </summary>
        string? Extension { get; }
    }
}
