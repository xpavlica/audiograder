﻿using AudioGrader.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace AudioGrader.Common
{
    public static class Constants
    {
        /// <summary>
        /// The minimum mean opinion score a grader can return 
        /// </summary>
        public const float MinMos = 1f;

        /// <summary>
        /// The maxiumum mean opinion score a grader can return 
        /// </summary>
        public const float MaxMos = 5f;
    }
}
