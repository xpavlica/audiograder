﻿using AudioGrader.Common.Interfaces;
using MoreLinq;
using NWaves.Signals;
using NWaves.Transforms;
using System;
using System.Threading.Tasks;
using System.Linq;
using AudioGrader.Common.Models;
using AudioGrader.Utilities.NWavesExtensions;
using System.Diagnostics.CodeAnalysis;

namespace AudioGrader.Preprocessors.RingingToneCutter
{
    public class RingingToneCutter : ISignalProcessor
    {
        const int FftSize = 1024;

        readonly IModuleConfigurationProvider configurationProvider;

        public RingingToneCutter(IModuleConfigurationProvider configurationProvider)
        {
            this.configurationProvider = configurationProvider;
        }


        public Task<DiscreteSignalDto?> Apply([NotNull]DiscreteSignalDto signalDto)
        {
            if (signalDto is null)
                throw new ArgumentNullException(nameof(signalDto));

            DiscreteSignal signal = signalDto.ToNWavesDiscreteSignal();
            Settings settings = configurationProvider.GetConfiguration<Settings>();

            var frames = signal.Samples.Batch((int)(settings.FrameSize * signal.SamplingRate));
            var framesEnumerator = frames.GetEnumerator();
            Fft fft = new Fft(FftSize);

            int lastToneIndex = 0;
            float ringToneRun = 0f;
            int frequencyStep = signal.SamplingRate / FftSize;
            float[] spectrum = new float[FftSize / 2 + 1];
            for (int i = 0; framesEnumerator.MoveNext(); i++)
            {
                fft.MagnitudeSpectrum(framesEnumerator.Current.ToArray(), spectrum);
                int maxIndex = spectrum.Index().MaxBy(x => x.Value).First().Key;
                float dominantFrequency = maxIndex * (signal.SamplingRate / (float)FftSize);

                if (Math.Abs(dominantFrequency - settings.RingingToneFrequency) < frequencyStep * 1.2f)
                    ringToneRun += settings.FrameSize;
                else
                    ringToneRun = 0f;

                if (ringToneRun > settings.RingingToneLength)
                    lastToneIndex = i;

                if ((i - lastToneIndex) * settings.FrameSize > settings.PauseLength)
                    break;
            }

            return Task.FromResult(new DiscreteSignalDto(signal.SamplingRate, frames.Skip(lastToneIndex + 1).SelectMany(x => x)))!;
        }
    }
}
