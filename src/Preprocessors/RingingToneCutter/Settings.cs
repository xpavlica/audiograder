﻿using System;
using System.Collections.Generic;
using System.Text;
using AudioGrader.Configuration;
using Newtonsoft.Json;

namespace AudioGrader.Preprocessors.RingingToneCutter
{
    public class Settings : JsonModuleConfigurationBase<Settings>
    {
        public override Type ModuleType => typeof(RingingToneCutter);

        /// <summary>
        /// The frequency in Hz of the dial tone.
        /// </summary>
        [JsonProperty]
        public float RingingToneFrequency { get; set; } = 425f;

        /// <summary>
        /// The length of the dial tone.
        /// </summary>
        [JsonProperty]
        public float RingingToneLength { get; set; } = 1f;

        /// <summary>
        /// The time in seconds between dial tones.
        /// </summary>
        [JsonProperty]
        public float PauseLength { get; set; } = 5f;

        /// <summary>
        /// The size of audio frames to apply frequency-domain analysis to.
        /// </summary>
        [JsonProperty]
        public float FrameSize { get; set; } = 0.25f;
    }
}
