﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using AudioGrader.Common.Interfaces;
using NWaves.Signals;
using FFMpegCore.FFMPEG;
using FFMpegCore.FFMPEG.Argument;
using FFMpegCore.FFMPEG.Exceptions;
using FFMpegCore.FFMPEG.Enums;
using NWaves.Audio;
using AudioGrader.Common.Models;
using System.Linq;
using AudioGrader.Utilities.NWavesExtensions;
using System.Diagnostics.CodeAnalysis;

namespace AudioGrader.Decoders.FFmpegDecoder
{
    public class FFmpegDecoder : IAudioDecoder
    {
        static readonly ArgumentContainer.Factory WavArgumentsFactory = new ArgumentContainer.Factory()
            .Add(new DisableChannelArgument(Channel.Video))
            .Add(new OverrideArgument());

        public Task<bool> Supports([NotNull]Stream stream)
        {
            ArgumentContainer.Factory argumentsFactory = WavArgumentsFactory.Clone()
                    .Add(new DurationArgument(TimeSpan.FromSeconds(1)));

            return ConvertStreamAndInvokeCallback(stream, argumentsFactory, convertedOutput => convertedOutput != null);
        }

        public Task<IEnumerable<DiscreteSignalDto>?> ToChannelSignals([NotNull]Stream stream)
        {
            ArgumentContainer.Factory argumentsFactory = WavArgumentsFactory.Clone();

            return ConvertStreamAndInvokeCallback(stream, argumentsFactory, convertedOutput =>
            {
                if (convertedOutput == null)
                    return null;

                using FileStream wavFile = File.OpenRead(convertedOutput.FullName);
                WaveFile waveFile;
                try
                {
                    waveFile = new WaveFile(wavFile, true);
                }
                catch (FormatException)
                {
                    return null;
                }

                return waveFile.Signals.Select(x => x.ToDto()).ToArray().AsEnumerable();
            });
        }

        async Task<FileInfo> DumpToTemporaryFile(Stream stream)
        {
            string? filePath = null;
            try
            {
                filePath = Path.GetTempFileName();
                using (Stream fileStream = File.OpenWrite(filePath))
                    await stream.CopyToAsync(fileStream);
                return new FileInfo(filePath);
            }
            catch (IOException)
            {
                if (filePath != null)
                    File.Delete(filePath);
                throw;
            }
        }

        Task<FileInfo?> Convert(ArgumentContainer.Factory argumentsFactory, FileInfo inputFile, FileInfo outputFile)
        {
            try
            {
                ArgumentContainer arguments = argumentsFactory
                    .Add(new InputArgument(inputFile))
                    .Add(new ForceFormatArgument(Format.Wav))
                    .Add(new AudioCodecArgument(AudioCodec.Pcm_S16LE))
                    .Add(new BitExactArgument())
                    .Add(new OutputArgument(outputFile))
                    .Build();

                FFMpeg converter = new FFMpeg();
                return Task.Run(() => converter.Convert(arguments).ToFileInfo())!;
            }
            catch (FFMpegException)
            {
                return Task.FromResult<FileInfo?>(null);
            }
        }

        async Task<CallbackResult> ConvertStreamAndInvokeCallback<CallbackResult>(
            Stream stream, 
            ArgumentContainer.Factory argumentsFactory,
            Func<FileInfo?, CallbackResult> callback)
        {
            FileInfo? streamDump = null;
            FileInfo? convertedOutput = null;
            try
            {
                streamDump = await DumpToTemporaryFile(stream);
                convertedOutput = new FileInfo(Path.GetTempFileName());

                return callback(await Convert(argumentsFactory, streamDump, convertedOutput).ConfigureAwait(false));
            }
            finally
            {
                if (streamDump != null)
                    File.Delete(streamDump.FullName);
                if (convertedOutput != null)
                    File.Delete(convertedOutput.FullName);
            }
        }
    }
}
