﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace AudioGrader.Decoders.WavDecoder
{
#pragma warning disable IDE0044 // Readonly modifier makes no sense for native structs

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    struct WavHeader
    {
        // RIFF Header

        /// <summary>
        /// Contains "RIFF"
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] riffHeader; // { 82, 73, 70, 70 }
        /// <summary>
        /// Size of the wav portion of the file, which follows the first 8 bytes. File size - 8
        /// </summary>
        public int wavSize;
        /// <summary>
        /// Contains "WAVE"
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] waveHeader; // { 87, 65, 86, 69 }

        // Format Header

        /// <summary>
        /// Contains "fmt "
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] fmtHeader; // { 102, 109, 116, 32 }
        /// <summary>
        /// Should be 16 for PCM
        /// </summary>
        public int chunkSize;
        /// <summary>
        /// Should be 1 for PCM. 3 for IEEE Float
        /// </summary>
        public short audioFormat;
        public short channelCount;
        public int sampleRate;
        /// <summary>
        /// Number of bytes per second. sample_rate * num_channels * Bytes Per Sample
        /// </summary>
        public int byteRate;
        /// <summary>
        /// num_channels * Bytes Per Sample
        /// </summary>
        public short sampleAlignment;
        /// <summary>
        /// Number of bits per sample
        /// </summary>
        public short bitDepth;

        // Data

        /// <summary>
        /// Contains "data"
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] dataHeader; // { 100, 97, 116, 97 }
        /// <summary>
        /// Number of bytes in data. Number of samples * num_channels * sample byte size
        /// </summary>
        public int dataBytes;

        /// <summary>
        /// Checks if the header is valid.wav 
        /// </summary>
        /// <returns>The header's validity</returns>
        public bool Validate()
        {
            return Encoding.ASCII.GetString(riffHeader) == "RIFF" &&
                Encoding.ASCII.GetString(waveHeader) == "WAVE" &&
                Encoding.ASCII.GetString(fmtHeader) == "fmt ";
        }
    }
#pragma warning restore IDE0044
}
