﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using AudioGrader.Common.Interfaces;
using NWaves.Signals;
using NWaves.Audio;
using AudioGrader.Utilities.NWavesExtensions;
using AudioGrader.Common.Models;
using System.Diagnostics.CodeAnalysis;

namespace AudioGrader.Decoders.WavDecoder
{
    public class WavDecoder : IAudioDecoder
    {
        /// <summary>
        /// Wav header size in bytes
        /// </summary>
        const int HeaderSize = 44;

        public async Task<bool> Supports([NotNull]Stream stream)
        {
            WavHeader header = await GetHeader(stream);
            return Supports(header);
        }

        bool Supports(WavHeader header) => header.Validate() && (header.audioFormat == 1 || header.audioFormat == 3) && WaveFile.SupportedBitDepths.Contains(header.bitDepth);

        async Task<WavHeader> GetHeader(Stream stream)
        {
            byte[] headerData = new byte[HeaderSize];
            await stream.ReadAsync(headerData, 0, HeaderSize);

            GCHandle? headerHandle = null;
            try
            {
                headerHandle = GCHandle.Alloc(headerData, GCHandleType.Pinned);
                WavHeader header = Marshal.PtrToStructure<WavHeader>(headerHandle.Value.AddrOfPinnedObject());
                return header;
            }
            finally
            {
                headerHandle?.Free();
            }
        }

        public Task<IEnumerable<DiscreteSignalDto>?> ToChannelSignals([NotNull]Stream stream)
        {
            WaveFile waveFile;
            try
            {
                waveFile = new WaveFile(stream, true);
            }
            catch (FormatException)
            {
                return Task.FromResult<IEnumerable<DiscreteSignalDto>?>(null);
            }

            return Task.FromResult(waveFile.Signals.Select(x => x.ToDto()).ToArray().AsEnumerable())!;
        }
    }
}
