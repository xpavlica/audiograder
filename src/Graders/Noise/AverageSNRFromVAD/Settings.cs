﻿using System;
using System.Collections.Generic;
using System.Text;
using AudioGrader.Configuration;
using AudioGrader.Utilities.VoiceActivityDetector;
using Newtonsoft.Json;

namespace AudioGrader.Graders.Noise.AverageSNRFromVAD
{
    public class Settings : JsonModuleConfigurationBase<Settings>
    {
        public override Type ModuleType => typeof(AverageSNRFromVAD);

        /// <summary>
        /// The maximum SNR value (in dB) which gets a MOS score of 1 (Bad)
        /// </summary>
        [JsonProperty]
        public float BadThreshold { get; set; } = 12.24f;

        /// <summary>
        /// The minimum SNR value (in dB) which gets a MOS score of 5 (Excellent)
        /// </summary>
        [JsonProperty]
        public float ExcellentThreshold { get; set; } = 70;

        [JsonProperty]
        public BaseThresholds VADThresholds { get; set; } = BaseThresholds.Default;
    }
}
