﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AudioGrader.Common.Interfaces;
using AudioGrader.Common.Models;
using AudioGrader.Utilities.NWavesExtensions;
using AudioGrader.Utilities.VoiceActivityDetector;
using NWaves.Signals;
using MoreLinq;
using AudioGrader.Common;
using System.Diagnostics.CodeAnalysis;

namespace AudioGrader.Graders.Noise.AverageSNRFromVAD
{
    public class AverageSNRFromVAD : IGrader
    {
        readonly IModuleConfigurationProvider configurationProvider;

        public AverageSNRFromVAD(IModuleConfigurationProvider configurationProvider)
        {
            this.configurationProvider = configurationProvider;
        }

        public Task<GraderResult?> Grade([NotNull] IEnumerable<DiscreteSignalDto> signals)
        {
            if (!signals.Any())
                throw new ArgumentException("No signal was provided.", nameof(signals));

            if (signals.Max(x => x.Samples.Length) == 0)
                throw new ArgumentException("All signals are zero-length.", nameof(signals));

            Settings settings = configurationProvider.GetConfiguration<Settings>();

            DiscreteSignal signal = signals.Select(x => x.ToNWavesDiscreteSignal()).DownmixToMono();
            bool[] voiceActivityMask = VoiceActivityDetector.Detect(signal, settings.VADThresholds);

            DiscreteSignal voiceSamples = new DiscreteSignal(signal.SamplingRate, signal.Samples.Index().Where(x => voiceActivityMask[x.Key]).Select(x => x.Value));
            DiscreteSignal noiseSamples = new DiscreteSignal(signal.SamplingRate, signal.Samples.Index().Where(x => !voiceActivityMask[x.Key]).Select(x => x.Value));

            float maxAbsoluteAmplitude = voiceSamples.Samples.Max(x => Math.Abs(x));
            float noiseAverageRms = noiseSamples.Rms();

            double snr;
            if (voiceSamples.Length == 0)
                snr = 0d;
            else if (noiseSamples.Length == 0)
                snr = settings.ExcellentThreshold;
            else
                snr = 20d * Math.Log10(maxAbsoluteAmplitude / noiseAverageRms);

            switch (snr)
            {
                case double bad when bad <= settings.BadThreshold:
                    return Task.FromResult(new GraderResult(Constants.MinMos))!;
                case double excellent when excellent >= settings.ExcellentThreshold:
                    return Task.FromResult(new GraderResult(Constants.MaxMos))!;
                default:
                    double mos = Constants.MinMos + (Constants.MaxMos - Constants.MinMos) * (snr - settings.BadThreshold) / (settings.ExcellentThreshold - settings.BadThreshold);
                    return Task.FromResult(new GraderResult((float)mos))!;
            }
        }
    }
}
