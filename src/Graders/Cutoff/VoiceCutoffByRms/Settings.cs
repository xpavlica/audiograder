﻿using System;
using System.Collections.Generic;
using System.Text;
using AudioGrader.Configuration;
using AudioGrader.Utilities.VoiceActivityDetector;
using Newtonsoft.Json;

namespace AudioGrader.Graders.Cutoff.VoiceCutoffByRms
{
    public class Settings : JsonModuleConfigurationBase<Settings>
    {
        public override Type ModuleType => typeof(VoiceCutoffByRms);

        [JsonProperty]
        public BaseThresholds VADThresholds { get; set; } = BaseThresholds.Default;

        /// <summary>
        /// The length (in seconds) at the end of the audio to calculate energy ratio off of.
        /// </summary>
        [JsonProperty]
        public float FrameSize { get; set; } = 0.15f;

        /// <summary>
        /// The minimum ratio of <i>(last frame mean energy) / (voice segments mean energy)</i> which still gets a MOS score of 1 (Bad)
        /// </summary>
        [JsonProperty]
        public float BadRatio { get; set; } = 0.6f;

        /// <summary>
        /// The maximum ratio of <i>(last frame mean energy) / (voice segments mean energy)</i> which still gets a MOS score of 5 (Excellent)
        /// </summary>
        [JsonProperty]
        public float ExcellentRatio { get; set; } = 0.01f;
    }
}
