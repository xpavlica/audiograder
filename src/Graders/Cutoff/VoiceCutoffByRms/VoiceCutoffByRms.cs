﻿using System;
using AudioGrader.Common.Interfaces;
using AudioGrader.Utilities.NWavesExtensions;
using AudioGrader.Utilities.VoiceActivityDetector;
using System.Threading.Tasks;
using AudioGrader.Common.Models;
using System.Collections.Generic;
using NWaves.Signals;
using System.Linq;
using AudioGrader.Common;
using System.Diagnostics.CodeAnalysis;

namespace AudioGrader.Graders.Cutoff.VoiceCutoffByRms
{
    public class VoiceCutoffByRms : IGrader
    {
        readonly IModuleConfigurationProvider configurationProvider;

        public VoiceCutoffByRms(IModuleConfigurationProvider configurationProvider)
        {
            this.configurationProvider = configurationProvider;
        }

        public Task<GraderResult?> Grade([NotNull] IEnumerable<DiscreteSignalDto> signals)
        {
            if (!signals.Any())
                throw new ArgumentException("No signal was provided.", nameof(signals));

            if (signals.Max(x => x.Samples.Length) == 0)
                throw new ArgumentException("All signals are zero-length.", nameof(signals));

            Settings settings = configurationProvider.GetConfiguration<Settings>();

            DiscreteSignal signal = signals.Select(x => x.ToNWavesDiscreteSignal()).DownmixToMono();

            bool[]? voiceMask = VoiceActivityDetector.Detect(signal, settings.VADThresholds);
            DiscreteSignal? voiceSignal = new DiscreteSignal(signal.SamplingRate, signal.Samples
                .Zip(voiceMask, (x, y) => (sample: x, voice: y))
                .Where(x => x.voice)
                .Select(x => x.sample));

            float voiceRms = voiceSignal.Rms();
            if (float.IsNaN(voiceRms))
                return Task.FromResult(new GraderResult(Constants.MinMos))!;

            voiceSignal = null;
            voiceMask = null;

            DiscreteSignal lastFrame = new DiscreteSignal(signal.SamplingRate, signal.Samples.TakeLast(Math.Min(signal.Length, (int)(signal.SamplingRate * settings.FrameSize))));
            float lastFrameRms = lastFrame.Rms();

            float ratio = lastFrameRms / voiceRms;
            float mos = Constants.MaxMos - (Constants.MaxMos - Constants.MinMos) * (ratio - settings.ExcellentRatio) / (settings.BadRatio - settings.ExcellentRatio);
            mos = Math.Min(Constants.MaxMos, Math.Max(Constants.MinMos, mos)); // Clamping between (MinMos, MaxMos)

            return Task.FromResult(new GraderResult(mos))!;
        }
    }
}
