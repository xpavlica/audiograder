﻿using System;
using System.Collections.Generic;
using System.Text;
using AudioGrader.Configuration;
using Newtonsoft.Json;

namespace AudioGrader.Graders.Clipping.ClippingHistogram
{
    public class Settings : JsonModuleConfigurationBase<Settings>
    {
        public override Type ModuleType => typeof(ClippingHistogram);

        /// <summary>
        /// The number of histogram buckets. The higher the number, the more sqaure-like must the frequency be to be counted as clipping.
        /// </summary>
        [JsonProperty]
        public int BucketCount { get; set; } = 80;

        /// <summary>
        /// The maximum ratio of <i>(biggest non-edge bucket) / (biggest edge bucket)</i> which still gets a MOS score of 1 (Bad)
        /// </summary>
        [JsonProperty]
        public float BadRatio { get; set; } = 0.7f;

        /// <summary>
        /// The minimum ratio of <i>(biggest non-edge bucket) / (biggest edge bucket)</i> which still gets a MOS score of 5 (Excellent)
        /// </summary>
        [JsonProperty]
        public float ExcellentRatio { get; set; } = 5f;
    }
}
