﻿using AudioGrader.Common.Interfaces;
using AudioGrader.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqStatistics;
using AudioGrader.Common;
using Serilog;
using static MoreLinq.Extensions.MaxByExtension;
using System.Diagnostics.CodeAnalysis;

namespace AudioGrader.Graders.Clipping.ClippingHistogram
{
    public class ClippingHistogram : IGrader
    {
        readonly IModuleConfigurationProvider configurationProvider;
        readonly ILogger logger;

        public ClippingHistogram(IModuleConfigurationProvider configurationProvider, ILogger logger)
        {
            this.configurationProvider = configurationProvider;
            this.logger = logger;
        }

        public Task<GraderResult?> Grade([NotNull] IEnumerable<DiscreteSignalDto> signals)
        {
            if (!signals.Any())
                throw new ArgumentException("No signal was provided.", nameof(signals));

            if (signals.Max(x => x.Samples.Length) == 0)
                throw new ArgumentException("All signals are zero-length.", nameof(signals));

            Settings settings = configurationProvider.GetConfiguration<Settings>();

            if (settings.BucketCount < 3)
            {
                logger.Warning("The number of buckets is less than 3, using 3 buckets instead.");
                settings.BucketCount = 3;
            }

            float ratio = signals.AsParallel().Select(x => CalculateBucketRatio(x, settings)).Min();

            float mos = Constants.MinMos + (Constants.MaxMos - Constants.MinMos) * (ratio - settings.BadRatio) / (settings.ExcellentRatio - settings.BadRatio);
            mos = Math.Min(Constants.MaxMos, Math.Max(Constants.MinMos, mos)); // Clamping between (MinMos, MaxMos)

            return Task.FromResult(new GraderResult(mos))!;
        }

        static float CalculateBucketRatio(DiscreteSignalDto signal, Settings settings)
        {
            var histogram = signal.Samples.Histogram(settings.BucketCount, BinningMode.MaxValueInclusive).ToArray();
            Bin edgeBin = histogram[0].Count > histogram[^1].Count ? histogram[0] : histogram[^1];
            Bin maxNonEdgeBin = histogram.Skip(1).SkipLast(1).MaxBy(x => x.Count).First();
            return maxNonEdgeBin.Count / (float)edgeBin.Count;
        }
    }
}
