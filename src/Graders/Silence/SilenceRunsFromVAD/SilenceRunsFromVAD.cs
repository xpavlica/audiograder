﻿using AudioGrader.Common.Interfaces;
using AudioGrader.Common.Models;
using AudioGrader.Utilities.NWavesExtensions;
using AudioGrader.Utilities.VoiceActivityDetector;
using NWaves.Signals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AudioGrader.Common;
using Linq.Extras;
using System.Diagnostics.CodeAnalysis;

namespace AudioGrader.Graders.Silence.SilenceRunsFromVAD
{
    public class SilenceRunsFromVAD : IGrader
    {
        readonly IModuleConfigurationProvider configurationProvider;

        public SilenceRunsFromVAD(IModuleConfigurationProvider configurationProvider)
        {
            this.configurationProvider = configurationProvider;
        }

        public Task<GraderResult?> Grade([NotNull] IEnumerable<DiscreteSignalDto> signals)
        {
            if (!signals.Any())
                throw new ArgumentException("No signal was provided.", nameof(signals));

            if (signals.Max(x => x.Samples.Length) == 0)
                throw new ArgumentException("All signals are zero-length.", nameof(signals));

            Settings settings = configurationProvider.GetConfiguration<Settings>();

            DiscreteSignal signal = signals.Select(x => x.ToNWavesDiscreteSignal()).DownmixToMono();
            var voiceActivityMask = VoiceActivityDetector.Detect(signal, settings.VADThresholds).ToList();

            int firstVoiceIndex = voiceActivityMask.IndexOf(true);
            int lastVoiceIndex = voiceActivityMask.LastIndexOf(true);

            if (firstVoiceIndex == -1)
                return Task.FromResult(new GraderResult(Constants.MinMos))!;

            // An array of lengths (in seconds) of silence runs in the middle of the signal
            var silenceSegments = voiceActivityMask
                .Skip(firstVoiceIndex)
                .SkipLast(voiceActivityMask.Count - lastVoiceIndex - 1)
                .GroupUntilChanged()
                .Where(x => !x.Key && x.Count() >= signal.SamplingRate * settings.SilenceRunThreshold)
                .Select(x => x.Count() / (float)signal.SamplingRate)
                .ToArray();

            if (silenceSegments.Length == 0)
                return Task.FromResult(new GraderResult(Constants.MaxMos))!;

            float countMos = GetSilenceCountMos(silenceSegments, settings);
            float averageLengthMos = GetAverageLengthMos(silenceSegments, settings);
            float ratioMos = GetRatioMos(silenceSegments, (lastVoiceIndex - firstVoiceIndex + 1) / (float)signal.SamplingRate, settings);

            float mos = (countMos * settings.RunCountWeight + averageLengthMos * settings.RunAverageLengthWeight + ratioMos * settings.SilenceRatioWeight) /
                (settings.RunCountWeight + settings.RunAverageLengthWeight + settings.SilenceRatioWeight);

            return Task.FromResult(new GraderResult(mos))!;
        }

        static float GetSilenceCountMos(float[] silenceSegments, Settings settings)
        {
            float mos = Constants.MaxMos - (Constants.MaxMos - Constants.MinMos) * 
                (silenceSegments.Length - settings.RunCountExcellentThreshold) / (settings.RunCountBadThreshold - settings.RunCountExcellentThreshold);
            mos = Math.Min(Constants.MaxMos, Math.Max(Constants.MinMos, mos));
            return mos;
        }

        static float GetAverageLengthMos(float[] silenceSegments, Settings settings)
        {
            float average = silenceSegments.Average();
            float mos = Constants.MaxMos - (Constants.MaxMos - Constants.MinMos) *
                (average - settings.RunAverageLengthExcellentThreshold) / (settings.RunAverageLengthBadThreshold - settings.RunAverageLengthExcellentThreshold);
            mos = Math.Min(Constants.MaxMos, Math.Max(Constants.MinMos, mos));
            return mos;
        }

        static float GetRatioMos(float[] silenceSegments, float totalLength, Settings settings)
        {
            float ratio = silenceSegments.Sum() / totalLength;
            float mos = Constants.MaxMos - (Constants.MaxMos - Constants.MinMos) * (ratio - settings.SilenceRatioExcellentThreshold) / (settings.SilenceRatioBadThreshold - settings.SilenceRatioExcellentThreshold);
            mos = Math.Min(Constants.MaxMos, Math.Max(Constants.MinMos, mos)); // Clamping between (MinMos, MaxMos)
            return mos;
        }
    }
}
