﻿using System;
using System.Collections.Generic;
using System.Text;
using AudioGrader.Configuration;
using AudioGrader.Utilities.VoiceActivityDetector;
using Newtonsoft.Json;

namespace AudioGrader.Graders.Silence.SilenceRunsFromVAD
{
    public class Settings : JsonModuleConfigurationBase<Settings>
    {
        public override Type ModuleType => typeof(SilenceRunsFromVAD);

        [JsonProperty]
        public BaseThresholds VADThresholds { get; set; } = BaseThresholds.Default;

        /// <summary>
        /// A duration (in seconds) of no voice activity to be considered as unnatural
        /// </summary>
        [JsonProperty]
        public float SilenceRunThreshold { get; set; } = 3f;
        
        /// <summary>
        /// The minimum count of silence runs which gets a MOS score of 1 (Bad)
        /// </summary>
        [JsonProperty]
        public float RunCountBadThreshold { get; set; } = 4;

        /// <summary>
        /// The maximum count of silence runs which gets a MOS score of 5 (Excellent)
        /// </summary>
        [JsonProperty]
        public float RunCountExcellentThreshold { get; set; } = 1;

        /// <summary>
        /// The weight of the count of silence runs on the final MOS grade
        /// </summary>
        [JsonProperty]
        public float RunCountWeight { get; set; } = 10f;

        /// <summary>
        /// The minimum average length of silence runs which gets a MOS score of 1 (Bad)
        /// </summary>
        [JsonProperty]
        public float RunAverageLengthBadThreshold { get; set; } = 8f;

        /// <summary>
        /// The maximum average length of silence runs which gets a MOS score of 5 (Excellent)
        /// </summary>
        [JsonProperty]
        public float RunAverageLengthExcellentThreshold { get; set; } = 4f;

        /// <summary>
        /// The weight of the average length of silence runs on the final MOS grade
        /// </summary>
        [JsonProperty]
        public float RunAverageLengthWeight { get; set; } = 5f;

        /// <summary>
        /// The minimum ratio of (silence runs) / (audio length) which gets a MOS score of 1 (Bad)
        /// </summary>
        [JsonProperty]
        public float SilenceRatioBadThreshold { get; set; } = 0.05f;

        /// <summary>
        /// The maximum ratio of (silence runs) / (audio length) which gets a MOS score of 5 (Excellent)
        /// </summary>
        [JsonProperty]
        public float SilenceRatioExcellentThreshold { get; set; } = 0.0005f;

        /// <summary>
        /// The weight of the ratio of (silence runs) / (audio length) on the final MOS grade
        /// </summary>
        [JsonProperty]
        public float SilenceRatioWeight { get; set; } = 10f;
    }
}
