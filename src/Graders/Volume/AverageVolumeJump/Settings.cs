﻿using System;
using System.Collections.Generic;
using System.Text;
using AudioGrader.Configuration;
using AudioGrader.Utilities.VoiceActivityDetector;
using Newtonsoft.Json;

namespace AudioGrader.Graders.Volume.AverageVolumeJump
{
    public class Settings : JsonModuleConfigurationBase<Settings>
    {
        public override Type ModuleType => typeof(AverageVolumeJump);

        [JsonProperty]
        public BaseThresholds VADThresholds { get; set; } = BaseThresholds.Default;

        /// <summary>
        /// The length (in seconds) of audio slices to compare volume similarity for. 
        /// </summary>
        [JsonProperty]
        public float AveragingInterval { get; set; } = 46.7f;

        /// <summary>
        /// The maximum ratio of adjacent slices's average volume levels which gets a MOS score of 1 (Bad)
        /// </summary>
        [JsonProperty]
        public float BadThreshold { get; set; } = 0.1f;

        /// <summary>
        /// The minimum ratio of adjacent slices's average volume levels which gets a MOS score of 5 (Excellent)
        /// </summary>
        [JsonProperty]
        public float ExcellentThreshold { get; set; } = 0.83f;
    }
}
