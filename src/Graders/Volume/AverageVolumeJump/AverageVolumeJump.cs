﻿using AudioGrader.Common;
using AudioGrader.Common.Interfaces;
using AudioGrader.Common.Models;
using AudioGrader.Utilities.NWavesExtensions;
using AudioGrader.Utilities.VoiceActivityDetector;
using Linq.Extras;
using NWaves.Signals;
using NWaves.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace AudioGrader.Graders.Volume.AverageVolumeJump
{
    /// <summary>
    /// Long-term average volume change detection (e.g. from a half-connected microphone jack)
    /// </summary>
    public class AverageVolumeJump : IGrader
    {
        readonly IModuleConfigurationProvider configurationProvider;

        public AverageVolumeJump(IModuleConfigurationProvider configurationProvider)
        {
            this.configurationProvider = configurationProvider;
        }

        public Task<GraderResult?> Grade([NotNull] IEnumerable<DiscreteSignalDto> signals)
        {
            if (!signals.Any())
                throw new ArgumentException("No signal was provided.", nameof(signals));

            if (signals.Max(x => x.Samples.Length) == 0)
                throw new ArgumentException("All signals are zero-length.", nameof(signals));

            Settings settings = configurationProvider.GetConfiguration<Settings>();

            DiscreteSignal signal = signals.Select(x => x.ToNWavesDiscreteSignal()).DownmixToMono();
            bool[] voiceMask = VoiceActivityDetector.Detect(signal, settings.VADThresholds);

            // Mean loudness (in dbA) of audio frames with voice activity per interval
            var loudnessByInterval = signal.Samples
                .Zip(voiceMask, (sample, mask) => (sample, mask))
                .Batch((int)(settings.AveragingInterval * signal.SamplingRate))
                .Select(x => x.Where(y => y.mask).Select(y => y.sample))
                .Select(x => new DiscreteSignal(signal.SamplingRate, x).Loudness())
                .Select(x => Scale.ToDecibelPower(x))
                .ToArray();

            var ratios = loudnessByInterval.Zip(loudnessByInterval.Skip(1), (first, second) => Math.Max(first, second) / Math.Min(first, second));
            double worstLoudnessRatio = ratios.Any() ? ratios.Min() : 1d;

            double mos = Constants.MinMos + (Constants.MaxMos - Constants.MinMos) *
                (worstLoudnessRatio - settings.BadThreshold) / (settings.ExcellentThreshold - settings.BadThreshold);
            mos = Math.Min(Constants.MaxMos, Math.Max(Constants.MinMos, mos));

            return Task.FromResult(new GraderResult((float)mos))!;
        }
    }
}
