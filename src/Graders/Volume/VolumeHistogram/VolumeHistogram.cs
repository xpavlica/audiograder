﻿using AudioGrader.Common.Interfaces;
using AudioGrader.Common.Models;
using AudioGrader.Utilities.NWavesExtensions;
using LinqStatistics;
using NWaves.Signals;
using NWaves.Transforms;
using NWaves.Utils;
using MoreLinq;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AudioGrader.Common;
using System.Diagnostics.CodeAnalysis;

namespace AudioGrader.Graders.Volume.VolumeHistogram
{
    /// <summary>
    /// Detects unbalanced volume levels in the voice recording. Assumes audio recordings with two active speakers on input.
    /// </summary>
    public class VolumeHistogram : IGrader
    {
        const int FftSize = 256;

        readonly IModuleConfigurationProvider configurationProvider;
        readonly ILogger logger;

        public VolumeHistogram(IModuleConfigurationProvider configurationProvider, ILogger logger)
        {
            this.configurationProvider = configurationProvider;
            this.logger = logger;
        }

        public Task<GraderResult?> Grade([NotNull] IEnumerable<DiscreteSignalDto> signals)
        {
            if (!signals.Any())
                throw new ArgumentException("No signal was provided.", nameof(signals));

            if (signals.Max(x => x.Samples.Length) == 0)
                throw new ArgumentException("All signals are zero-length.", nameof(signals));

            Settings settings = configurationProvider.GetConfiguration<Settings>();

            if (settings.BucketCount < 3)
            {
                logger.Warning("The number of buckets is less than 3, using 3 buckets instead.");
                settings.BucketCount = 3;
            }

            DiscreteSignal signal = signals.Select(x => x.ToNWavesDiscreteSignal()).DownmixToMono();
            // Loudness of audio frames in dbAFS (with reference value of 1 - for normalized signals the values span from -Inf to 0)
            List<double> loudnessbyFrame = signal.Samples
                .Batch((int)(signal.SamplingRate * settings.FrameSize))
                .AsParallel()
                .AsOrdered()
                .Select(x => new DiscreteSignal(signal.SamplingRate, x).Loudness())
                .Select(x => Scale.ToDecibelPower(x))
                .ToList();

            var histogram = loudnessbyFrame.Histogram(settings.BucketCount, BinningMode.MaxValueInclusive);
            histogram = histogram.Where(x => x.RepresentativeValue >= settings.SilenceThreshold); // Skipping the buckets with silent frames
            if (!histogram.Any())
                return Task.FromResult(new GraderResult(Constants.MaxMos))!; // Only silent frames, nothing to grade

            // Counting only above-median part of histogram bins to suppress the influence of noise on the volume ratio
            int median = (int)histogram.Select(x => x.Count).Median();
            histogram = histogram.Select(x => new Bin(x.RepresentativeValue, x.Range.Min, x.Range.Max, Math.Max(0, x.Count - median), true));

            // Detecting two independent speakers based on volume difference
            var indexedHistogram = histogram.Index();
            var maximumBin = indexedHistogram.MaxBy(x => x.Value.Count).First();
            var secondBin = indexedHistogram.Where(x => x.Key != maximumBin.Key).MaxBy(x => x.Value.Count).First();

            double volumeDifference = Math.Abs(maximumBin.Value.RepresentativeValue - secondBin.Value.RepresentativeValue);
            float ratio = secondBin.Value.Count / (float)maximumBin.Value.Count;
            if (volumeDifference < settings.VolumeDifferenceThreshold || ratio < settings.BucketSizeRatioThreshold)
                volumeDifference = 0d;

            double mos = Constants.MaxMos - (Constants.MaxMos - Constants.MinMos) *
                (volumeDifference - settings.ExcellentThreshold) / (settings.BadThreshold - settings.ExcellentThreshold);
            mos = Math.Min(Constants.MaxMos, Math.Max(Constants.MinMos, mos));
            return Task.FromResult(new GraderResult((float)mos))!;
        }

#if DEBUG
        public static IEnumerable<Bin> GetHistogram
            (IEnumerable<DiscreteSignal> signals, Settings settings)
        {
            if (!signals.Any())
                throw new ArgumentException("No signal was provided.", nameof(signals));

            if (signals.Max(x => x.Length) == 0)
                throw new ArgumentException("All signals are zero-length.", nameof(signals));

            DiscreteSignal signal = signals.DownmixToMono();
            // Loudness of audio frames in dbA (with reference value of 1 - for normalized signals the values span from -Inf to 0)
            List<double> loudnessbyFrame = signal.Samples
                .Batch((int)(signal.SamplingRate * settings.FrameSize))
                .AsParallel()
                .AsOrdered()
                .Select(x => new DiscreteSignal(signal.SamplingRate, x).Loudness())
                .Select(x => Scale.ToDecibelPower(x))
                .ToList();

            var histogram = loudnessbyFrame.Histogram(settings.BucketCount, BinningMode.MaxValueInclusive);
            if (histogram.First().RepresentativeValue < settings.SilenceThreshold)
                histogram = histogram.Skip(1); // Skipping the bucket with silent frames

            // Counting only above-median part of histogram bins to suppress the influence of noise
            int median = (int)histogram.Select(x => x.Count).Median();
            histogram = histogram.Select(x => new Bin(x.RepresentativeValue, x.Range.Min, x.Range.Max, Math.Max(0, x.Count - median), true));

            // Detecting two independent speakers based on volume difference
            var indexedHistogram = histogram.Index();
            var maximumBin = indexedHistogram.MaxBy(x => x.Value.Count).First();
            var secondBin = indexedHistogram.Where(x => x.Key != maximumBin.Key).MaxBy(x => x.Value.Count).First();

            double volumeDifference = Math.Abs(maximumBin.Value.RepresentativeValue - secondBin.Value.RepresentativeValue);
            float ratio = secondBin.Value.Count / (float)maximumBin.Value.Count;
            if (volumeDifference < settings.VolumeDifferenceThreshold || ratio < settings.BucketSizeRatioThreshold)
                volumeDifference = 0d;

            double mos = Constants.MaxMos - (Constants.MaxMos - Constants.MinMos) *
                (volumeDifference - settings.ExcellentThreshold) / (settings.BadThreshold - settings.ExcellentThreshold);
            mos = Math.Min(Constants.MaxMos, Math.Max(Constants.MinMos, mos));

            return histogram;
        }
#endif
    }
}
