﻿using System;
using System.Collections.Generic;
using System.Text;
using AudioGrader.Configuration;
using Newtonsoft.Json;

namespace AudioGrader.Graders.Volume.VolumeHistogram
{
    public class Settings : JsonModuleConfigurationBase<Settings>
    {
        public override Type ModuleType => typeof(VolumeHistogram);

        /// <summary>
        /// The length (in seconds) of volume calculation frames.
        /// </summary>
        [JsonProperty]
        public float FrameSize { get; set; } = 0.42f;

        /// <summary>
        /// The number of histogram buckets. The higher the number, the higher the sensitivity.
        /// </summary>
        [JsonProperty]
        public int BucketCount { get; set; } = 6;

        /// <summary>
        /// The volume threhold for frame to be counted as silence.
        /// </summary>
        [JsonProperty]
        public float SilenceThreshold { get; set; } = -77.9f;

        /// <summary>
        /// The difference in dbA between the two biggest volume histogram buckets to be considered as originating from differing audio sources.
        /// </summary>
        [JsonProperty]
        public float VolumeDifferenceThreshold { get; set; } = 3.92f;

        /// <summary>
        /// The minimum above-median bucket size ratio (how often this volume occurs in the signal) 
        /// of the two biggest volume histogram buckets to be considered as originating from differing audio sources.
        /// </summary>
        [JsonProperty]
        public float BucketSizeRatioThreshold { get; set; } = 0.15f;

        /// <summary>
        /// The maximum difference in dbA between the two biggest volume histogram buckets which gets a MOS score of 1 (Bad)
        /// </summary>
        [JsonProperty]
        public float BadThreshold { get; set; } = 27.25f;

        /// <summary>
        /// The minimum difference in dbA between the two biggest volume histogram buckets which gets a MOS score of 5 (Excellent)
        /// </summary>
        [JsonProperty]
        public float ExcellentThreshold { get; set; } = 1.59f;
    }
}
