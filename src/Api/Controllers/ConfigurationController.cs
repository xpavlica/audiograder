﻿using AudioGrader.Api.Configuration;
using AudioGrader.Api.Modules;
using AudioGrader.Common.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using McMaster.NETCore.Plugins;
using System.IO;
using Microsoft.AspNetCore.Routing;
using System.Runtime.CompilerServices;

namespace AudioGrader.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ConfigurationController : ControllerBase
    {
        private readonly IModuleConfigurationProvider configurationProvider;
        private readonly IModuleLoader moduleLoader;

        const BindingFlags Flags = BindingFlags.Public | BindingFlags.Instance;

        public ConfigurationController(IModuleConfigurationProvider configurationProvider, IModuleLoader moduleLoader)
        {
            this.configurationProvider = configurationProvider;
            this.moduleLoader = moduleLoader;
        }

        [HttpGet]
        public async Task<IActionResult> GetDefaultConfiguration([FromQuery(Name = "module")] string? moduleName = null)
        {
            if (string.IsNullOrEmpty(moduleName))
                return Ok(configurationProvider.GetConfiguration<ApiConfiguration>());
            return await GetModuleConfiguration(moduleName).ConfigureAwait(false);
        }

        public async Task<IActionResult> GetModuleConfiguration(string moduleName)
        {
            object? moduleInstance = moduleLoader.LoadModule(moduleName);

            if (moduleInstance is null)
                return BadRequest();

            // This theoretically should be able to be called synchronously, but it deadlocks the Visual Studio debugger on synchronous execution.
            // Works fine synchronously without a debugger.
            var configurationTypes = await Task.Run(() => moduleInstance.GetType().Assembly.GetTypes()
                    .Where(x => x.GetInterfaces()
                        .Where(y => y.IsGenericType)
                        .Select(y => y.GetGenericTypeDefinition())
                        .Contains(typeof(IModuleConfiguration<>)))
                    .ToDictionary(
                        x => x.FullName ?? x.Name, 
                        x => GetConfiguration(x.GetProperty(nameof(IModuleConfiguration<ApiConfiguration>.ModuleType), Flags)?
                            .GetValue(Activator.CreateInstance(x)) as Type)))
                .ConfigureAwait(false);


            return Ok(configurationTypes);
        }

        // An ugly hack to force the given module's code into the current context so that the correct configuration class for the given type can be found and serialized.
        // This assumes there is only one configuration class for each type.
        [MethodImpl(MethodImplOptions.NoInlining)]
        object? GetConfiguration(Type? type)
        {
            if (type is null || type.FullName is null)
                return null;

            string? modulePath = moduleLoader.GetModuleAssemblyFilePath(type.FullName);

            if (modulePath is null)
                return null;

            using PluginLoader pluginLoader = PluginLoader.CreateFromAssemblyFile(modulePath, x =>
            {
                x.PreferSharedTypes = true;
                x.LoadInMemory = true;
                x.IsUnloadable = true;
            });

            using (pluginLoader.EnterContextualReflection())
            {
                Assembly assembly = pluginLoader.LoadDefaultAssembly();
                type = assembly.GetType(type.FullName);
                if (type is null)
                    return null;

                var settingsType = assembly.GetTypes()
                    .Where(x =>
                        x.GetInterfaces()
                            .Any(y =>
                                y.IsGenericType &&
                                y.GetGenericTypeDefinition() == typeof(IModuleConfiguration<>)))
                    .Select(x => Activator.CreateInstance(x))
                    .FirstOrDefault(x => type == x?.GetType().GetProperty(nameof(IModuleConfiguration<ApiConfiguration>.ModuleType))?.GetValue(x) as Type)?
                    .GetType();

                if (settingsType is null)
                    return null;

                return typeof(IModuleConfigurationProvider)
                    .GetMethod(nameof(IModuleConfigurationProvider.GetConfiguration), Flags)?
                    .GetGenericMethodDefinition()
                    .MakeGenericMethod(new[] { settingsType })
                    .Invoke(configurationProvider, null);
            }
        }
    }
}
