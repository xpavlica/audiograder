﻿using AudioGrader.Api.Configuration;
using AudioGrader.Api.Enums;
using AudioGrader.Api.Models;
using AudioGrader.Api.Modules;
using AudioGrader.Common;
using AudioGrader.Common.Interfaces;
using AudioGrader.Common.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Core;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AudioGrader.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GradeController : ControllerBase
    {
        private readonly IModuleConfigurationProvider configurationProvider;
        private readonly IModuleLoader moduleLoader;
        private readonly GradeSynthesizer gradeSynthesizer;
        private readonly GradingResultCache cache;

        public GradeController(IModuleConfigurationProvider configurationProvider, IModuleLoader moduleLoader, GradingResultCache cache)
        {
            this.configurationProvider = configurationProvider;
            this.moduleLoader = moduleLoader;
            gradeSynthesizer = new GradeSynthesizer(moduleLoader);
            this.cache = cache;
        }

        [HttpGet]
        public IActionResult GetCachedResult([FromQuery] Guid guid)
        {
            if (cache.TryGetValue(guid, out var result))
                return result.ActionResult ?? Ok(result);
            return NotFound();
        }

        [HttpPost("form")]
        public async Task<IActionResult> FromFormData([FromForm] IFormFile file, [FromQuery(Name = "configuration")] ApiConfiguration? configuration = null)
        {
            using Stream stream = file.OpenReadStream();
            return await GradeRequest(stream, configuration);
        }

        [HttpPost]
        public async Task<IActionResult> FromBinaryBody([FromQuery(Name = "configuration")] ApiConfiguration? configuration = null)
        {
            using var stream = new MemoryStream();
            await Request.Body.CopyToAsync(stream);
            return await GradeRequest(stream, configuration);
        }

        async Task<IActionResult> GradeRequest(Stream audio, ApiConfiguration? configuration = null)
        {
            configuration ??= configurationProvider.GetConfiguration<ApiConfiguration>();

            GradingResult result = new GradingResult();
            cache.TryAdd(result.Id, result);
            var gradingTask = Grade(audio, result, configuration);

            await Task.WhenAny(gradingTask, Task.Delay(configuration.Timeout));
            return GetCachedResult(result.Id);
        }

        [NonAction]
        public async Task Grade(Stream audio, GradingResult result, ApiConfiguration? configuration = null)
        {
            result.State = GradingState.Starting;
            if (audio == null)
            {
                result.State = GradingState.Error;
                result.ActionResult = BadRequest("No audio data provided.");
                return;
            }

            if (!audio.CanSeek)
            {
                result.State = GradingState.Error;
                throw new ArgumentException("The audio stream has to support seeking", nameof(audio));
            }

            configuration ??= configurationProvider.GetConfiguration<ApiConfiguration>();

            if (!configuration.Decoders.Any())
            {
                result.State = GradingState.Error;
                result.ActionResult = BadRequest("No decoders provided.");
                return;
            }

            if (!configuration.GraderWeights.Any())
            {
                result.State = GradingState.Error;
                result.ActionResult = BadRequest("No graders provided.");
                return;
            }


            Log.Logger.Information("Choosing a decoder module.");
            result.State = GradingState.Decoding;
            IEnumerable<DiscreteSignalDto>? channels = await DecodeStream(audio, configuration).ConfigureAwait(false);
            if (channels is null)
            {
                Log.Information("No decoder supports the given audio stream.");
                result.State = GradingState.Error;
                result.ActionResult = StatusCode(415); // Unsupported Media Type
                return;
            }

            Log.Logger.Information("Preprocessing the given signal.");
            result.State = GradingState.Preprocessing;
            var preprocessTasks = channels.Select(x => PreprocessSignal(x, configuration)).ToArray();
            await Task.WhenAll(preprocessTasks).ConfigureAwait(false);
            channels = preprocessTasks.Select(x => x.Result);

            Log.Logger.Information("Grading the given signal.");
            result.State = GradingState.Grading;
            FinalGrade? grade = await gradeSynthesizer.Grade(channels, configuration.GraderWeights).ConfigureAwait(false);

            if (grade is null)
            {
                result.State = GradingState.Error;
                result.ActionResult = StatusCode(500);
                return;
            }

            Log.Logger.Verbose("Returning a final grade.");
            result.State = GradingState.Done;
            result.ActionResult = Ok(result);
            result.Grade = grade;
        }

        async Task<IEnumerable<DiscreteSignalDto>?> DecodeStream(Stream audio, ApiConfiguration configuration)
        {
            IEnumerable<DiscreteSignalDto>? channels = null;

            foreach (string typeName in configuration.Decoders)
            {
                IAudioDecoder? decoder = moduleLoader.LoadModule<IAudioDecoder>(typeName);
                if (decoder is null)
                {
                    Log.Logger.Warning($"No decoder named {typeName} was found.");
                    continue;
                }

                audio.Position = 0;
                if (!await decoder.Supports(audio).ConfigureAwait(false))
                    continue;

                Log.Logger.Information($"Using decoder named {typeName}.");

                audio.Position = 0;
                try
                {
                    channels = await decoder.ToChannelSignals(audio).ConfigureAwait(false);

                    if (channels is null)
                        Log.Logger.Warning($"{typeName} was unable to convert the given file.");
                    else
                        break;
                }
                catch (Exception ex)
                {
                    Log.Logger.Error(ex, $"An exception was thrown while decoding an audio stream with a decoder module named {typeName}.");
                }
            }

            return channels;
        }

        async Task<DiscreteSignalDto> PreprocessSignal(DiscreteSignalDto signal, ApiConfiguration configuration)
        {
            foreach (string typeName in configuration.Preprocessors)
            {
                ISignalProcessor? preprocessor = moduleLoader.LoadModule<ISignalProcessor>(typeName);
                if (preprocessor is null)
                {
                    Log.Logger.Warning($"No preprocessor named {typeName} was found.");
                    continue;
                }

                Log.Logger.Information($"Using preprocessor named {typeName}.");

                try
                {
                    DiscreteSignalDto? transformed = await preprocessor.Apply(signal).ConfigureAwait(false);
                    if (transformed is null)
                        Log.Logger.Warning($"{typeName} was unable to preprocess the given signal.");
                    signal = transformed ?? signal;
                }
                catch (Exception ex)
                {
                    Log.Logger.Error(ex, $"An exception was thrown while preprocessing a signal with a module named {typeName}.");
                }
            }

            return signal;
        }
    }
}
