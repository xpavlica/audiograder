﻿using AudioGrader.Api.Models;
using AudioGrader.Api.Modules;
using AudioGrader.Common.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace AudioGrader.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ModulesController : ControllerBase
    {
        private readonly IModuleLoader moduleLoader;

        public ModulesController(IModuleLoader moduleLoader)
        {
            this.moduleLoader = moduleLoader;
        }

        public IActionResult GetModules()
        {
            AvailableModules availableModules = new AvailableModules()
            {
                Decoders = moduleLoader.ListModules<IAudioDecoder>(),
                Preprocessors = moduleLoader.ListModules<ISignalProcessor>(),
                Graders = moduleLoader.ListModules<IGrader>()
            };
            return Ok(availableModules);
        }
    }
}
