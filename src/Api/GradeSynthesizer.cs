﻿using AudioGrader.Api.Models;
using AudioGrader.Api.Modules;
using AudioGrader.Common.Interfaces;
using AudioGrader.Common.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudioGrader.Api
{
    public class GradeSynthesizer
    {
        readonly IModuleLoader moduleLoader;

        public GradeSynthesizer(IModuleLoader moduleLoader)
        {
            this.moduleLoader = moduleLoader;
        }

        public async Task<FinalGrade?> Grade(IEnumerable<DiscreteSignalDto> channels, Dictionary<string, float> graderWeights)
        {
            Dictionary<string, GraderResult> scores = new Dictionary<string, GraderResult>();

            List<Task> tasks = new List<Task>();

            foreach (string typeName in graderWeights.Keys)
            {
                IGrader? grader = null;
                try
                {
                    grader = moduleLoader.LoadModule<IGrader>(typeName);
                }
                catch (Exception ex)
                {
                    Log.Logger.Error(ex, $"Error loading grading module named {typeName}.");
                    continue;
                }

                if (grader is null)
                {
                    Log.Logger.Warning($"No grader named {typeName} was found.");
                    continue;
                }

                // Run the grading asynchronously on a new thread
                Task gradingTask = Task.Run(async () =>
                {
                    try
                    {
                        GraderResult? result = await grader.Grade(channels).ConfigureAwait(false);
                        if (result is null)
                            Log.Logger.Warning($"The grader {typeName} returned null");
                        else
                            scores[typeName] = result;
                    }
                    catch (Exception ex)
                    {
                        Log.Fatal(ex, $"An exception occured in a grader module named {typeName}.");
                    }
                }) ;
                tasks.Add(gradingTask);
            }

            await Task.WhenAll(tasks).ConfigureAwait(false);

            if (!scores.Any())
                return null;

            float finalGrade = scores.Sum(x => x.Value.Score * graderWeights[x.Key]) / scores.Sum(x => graderWeights[x.Key]);

            return new FinalGrade(finalGrade, scores);
        }
    }
}
