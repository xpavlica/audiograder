﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace AudioGrader.Api.Modules
{
    public interface IModuleLoader
    {
        /// <summary>
        /// Loads the module named <paramref name="typeName"/>. Returns the first module with the given name. 
        /// If an exact match is required, provide a fully qualified <paramref name="typeName"/>.
        /// Throws <see cref="NotSupportedException"/> if an empty contructor is not found.
        /// </summary>
        /// <param name="typeName">The name of</param>
        /// <returns>An instance of the type created using a default constructor. Null if no type named <paramref name="typeName"/> is found.</returns>
        object? LoadModule(string typeName);

        /// <summary>
        /// Loads the module named <paramref name="typeName"/> implementing <typeparamref name="T"/>. Returns the first module with the given name. 
        /// If an exact match is required, provide a fully qualified <paramref name="typeName"/>.
        /// Throws <see cref="MissingMethodException"/> if an empty public contructor is not found.
        /// </summary>
        /// <param name="typeName">The name of</param>
        /// <returns>An instance of <typeparamref name="T"/> created using a default constructor. The default value of <typeparamref name="T"/> if no type named <paramref name="typeName"/> is found.</returns>
        [return: MaybeNull]
        T? LoadModule<T>(string typeName) where T : class;

        /// <summary>
        /// Lists all available types across all the modules.
        /// </summary>
        /// <returns>A collection of fully qualified type names.</returns>
        IEnumerable<string> ListModules();

        /// <summary>
        /// Lists all available types of type <typeparamref name="T"/> or implementing interface <typeparamref name="T"/> across all the modules.
        /// </summary>
        /// <typeparam name="T">The type or interface to be assignable from. Do note that not all types are shared across load contexts.</typeparam>
        /// <returns>A collection of fully qualified type names.</returns>
        IEnumerable<string> ListModules<T>();

        /// <summary>
        /// Returns the path to the assembly file containing a type named <paramref name="moduleName"/>. If one is not found, returns <see cref="null"/>.
        /// </summary>
        /// <param name="moduleName">The (possibly fully qualified) type name.</param>
        /// <returns>The assembly file path.</returns>
        string? GetModuleAssemblyFilePath(string moduleName);
    }
}
