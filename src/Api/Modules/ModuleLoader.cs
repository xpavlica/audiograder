﻿using AudioGrader.Api.Comparers;
using AudioGrader.Common.Interfaces;
using McMaster.NETCore.Plugins;
using Microsoft.Collections.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Reactive.Bindings;
using Serilog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Loader;
using System.Threading;
using System.Threading.Tasks;

namespace AudioGrader.Api.Modules
{
    /// <summary>
    /// A default implementation of a module loader. This class expects modules to be located in the folder named <i>Modules</i> in the server's root directory.
    /// Do note that this loader may crash during a debugging sesssion on older versions of .NET Core 3.1 due to a debug callback issue in the CLR. (https://github.com/dotnet/runtime/issues/2317).
    /// </summary>
    public class ModuleLoader : IModuleLoader
    {
        static readonly DirectoryInfo ModulesDirectory = new DirectoryInfo(
            Path.Combine(
                Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().GetName().CodeBase ?? ".").LocalPath) ?? ".",
                "Modules")
            );

        static readonly Type[] CommonTypes = typeof(IGrader).Assembly.GetTypes().Union(new[] { typeof(ILogger) }).ToArray();

        /// <summary>
        /// Type name <-> Dll path
        /// </summary>
        readonly ConcurrentDictionary<string, string> typePaths = new ConcurrentDictionary<string, string>();

        /// <summary>
        /// Fully qualified type name <-> Dll path
        /// </summary>
        readonly ConcurrentDictionary<string, string> fullyQualifiedTypePaths = new ConcurrentDictionary<string, string>();

        /// <summary>
        /// Fully qualified type name <-> types from <see cref="CommonTypes"/> that the given type can be assigned to
        /// </summary>
        readonly MultiValueDictionary<string, Type> assignableTypes = new MultiValueDictionary<string, Type>();

        /// <summary>
        /// Dll path <-> loader
        /// </summary>
        readonly ConcurrentDictionary<string, PluginLoader> loaders = new ConcurrentDictionary<string, PluginLoader>();
        /// <summary>
        /// A multi-value dictionary storing active instances created using the given <see cref="PluginLoader"/>.
        /// </summary>
        readonly MultiValueDictionary<PluginLoader, WeakReference> loaderReferences = new MultiValueDictionary<PluginLoader, WeakReference>(ReferenceEqualityComparer.Default);

        readonly FileSystemWatcher watcher = new FileSystemWatcher(ModulesDirectory.FullName);
        readonly ReactiveProperty<bool> modulesDirty = new ReactiveProperty<bool>(true);
        readonly ReaderWriterLockSlim rwLock = new ReaderWriterLockSlim();
        readonly IServiceProvider serviceProvider;

        public ModuleLoader(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;

            watcher.EnableRaisingEvents = true;
            watcher.Changed += (obj, args) => modulesDirty.Value = true;
            watcher.Created += (obj, args) => modulesDirty.Value = true;
            watcher.Deleted += (obj, args) => modulesDirty.Value = true;
            watcher.Renamed += (obj, args) => modulesDirty.Value = true;

            modulesDirty
                .ObserveOn(TaskPoolScheduler.Default)
                .Where(x => x)
                .Throttle(TimeSpan.FromSeconds(5f))
                .Subscribe(x => RefreshTypeCache());

            RefreshTypeCache();
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        void RefreshTypeCache()
        {
            if (!modulesDirty.Value)
                return;

            rwLock.EnterWriteLock();

            try
            {
                if (!modulesDirty.Value)
                    return;

                typePaths.Clear();
                fullyQualifiedTypePaths.Clear();
                assignableTypes.Clear();
                foreach (var dllPath in ModulesDirectory.EnumerateDirectories().SelectMany(x => x.EnumerateFiles()).Where(x => x.Extension == ".dll"))
                {
                    var loader = PluginLoader.CreateFromAssemblyFile(dllPath.FullName, true, CommonTypes, x => x.LoadInMemory = true);

                    using (loader.EnterContextualReflection())
                    {
                        foreach (Type type in loader.LoadDefaultAssembly().GetTypes())
                        {
                            // ToString() is called so that we do not store a reference to the string, hence blocking unloading of the assembly
                            typePaths.TryAdd(type.Name.ToString(), dllPath.FullName);
                            if (type.FullName != null)
                            {
                                fullyQualifiedTypePaths.TryAdd(type.FullName.ToString(), dllPath.FullName);
                                assignableTypes.AddRange(type.FullName.ToString(), CommonTypes.Where(x => x.IsAssignableFrom(type)));
                            }
                        }
                    }

                    loader.Dispose();
                }

                RefreshLoaderReferences();

                // Dispose of loaders with no active instances
                foreach (var record in loaders.Where(x => !loaderReferences.TryGetValue(x.Value, out _)))
                    record.Value.Dispose(); // Can lead to double-disposes on used loaders, but the library guards against this.

                loaders.Clear();
                modulesDirty.Value = false;

            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "An exception occured while caching plugins.");
                throw;
            }
            finally
            {
                rwLock.ExitWriteLock();
            }
        }

        void RefreshLoaderReferences()
        {
            PluginLoader[] toDelete = loaderReferences.Where(x => x.Value.All(y => !y.IsAlive)).Select(x => x.Key).ToArray();

            foreach (PluginLoader loader in toDelete)
            {
                loaderReferences.Remove(loader);
                loader.Dispose();
            }
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public object? LoadModule(string typeName) => LoadModule<object>(typeName);

        [MethodImpl(MethodImplOptions.NoInlining)]
        [return: MaybeNull]
        public T? LoadModule<T>(string typeName) where T : class
        {
            ModulesDirectory.Create();

            rwLock.EnterReadLock();

            try
            {
                fullyQualifiedTypePaths.TryGetValue(typeName, out string? dllPath);
                if (dllPath is null)
                    typePaths.TryGetValue(typeName, out dllPath);
                if (dllPath is null)
                    return default;

                if (!loaders.TryGetValue(dllPath, out PluginLoader? loader))
                {
                    loader = PluginLoader.CreateFromAssemblyFile(dllPath, true, CommonTypes, x => x.LoadInMemory = true);
                    loaders[dllPath] = loader;
                }


                Type moduleType = loader.LoadDefaultAssembly().GetTypes().FirstOrDefault(x => typeof(T).IsAssignableFrom(x) && !x.IsAbstract && (x.FullName == typeName || x.Name == typeName));

                if (moduleType is null)
                    return default;

                T instance;
                try
                {
                    instance = (T)ActivatorUtilities.CreateInstance(serviceProvider, moduleType);
                    loaderReferences.Add(loader, new WeakReference(instance));
                }
                catch (InvalidOperationException)
                {
                    Log.Logger.Error($"Could not find a matching contructor for the module named {typeName}. Are all the required services registered?");
                    return default;
                }

                return instance;
            }
            finally
            {
                rwLock.ExitReadLock();
            }
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public IEnumerable<string> ListModules()
        {
            rwLock.EnterReadLock();
            try
            {
                return fullyQualifiedTypePaths.Keys.ToImmutableArray();
            }
            finally
            {
                rwLock.ExitReadLock();
            }
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public IEnumerable<string> ListModules<T>()
        {
            rwLock.EnterReadLock();
            try
            {
                return assignableTypes.Where(x => x.Value.Contains(typeof(T))).Select(x => x.Key).ToImmutableArray();
            }
            finally
            {
                rwLock.ExitReadLock();
            }
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public string? GetModuleAssemblyFilePath(string moduleName)
        {
            rwLock.EnterReadLock();
            try
            {
                if (fullyQualifiedTypePaths.TryGetValue(moduleName, out string? path))
                    return path;
                if (typePaths.TryGetValue(moduleName, out path))
                    return path;
                return null;
            }
            finally
            {
                rwLock.ExitReadLock();
            }
        }

        ~ModuleLoader()
        {
            if (rwLock != null)
                rwLock.Dispose();
        }
    }
}
