﻿using AudioGrader.Api.Enums;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Nito.AsyncEx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudioGrader.Api.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class GradingResult
    {
        [JsonProperty]
        public Guid Id { get; } = Guid.NewGuid();

        [JsonProperty]
        public GradingState State { get; set; } = GradingState.Starting;

        [JsonProperty]
        public string StateDescription => State.ToString();

        [JsonProperty]
        public bool Done => State == GradingState.Done;

        [JsonProperty]
        public FinalGrade? Grade { get; set; }

        public AsyncAutoResetEvent Completed { get; } = new AsyncAutoResetEvent(false);

        public DateTime StartTime { get; } = DateTime.Now;

        public IActionResult? ActionResult { get; set; } 
    }
}
