﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudioGrader.Api.Models
{
    [JsonObject]
    public class AvailableModules
    {
        [JsonProperty]
        public IEnumerable<string> Decoders { get; set; } = Enumerable.Empty<string>();

        [JsonProperty]
        public IEnumerable<string> Preprocessors { get; set; } = Enumerable.Empty<string>();

        [JsonProperty]
        public IEnumerable<string> Graders { get; set; } = Enumerable.Empty<string>();
    }
}
