﻿using AudioGrader.Common.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.CodeAnalysis;
using AudioGrader.Common.Models;

namespace AudioGrader.Api.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class FinalGrade
    {
        [JsonProperty]
        public float Score { get; }

        [JsonProperty]
        public IDictionary<string, GraderResult> GraderScores { get; }

        public FinalGrade(float finalScore, IDictionary<string, GraderResult> graderScores)
        {
            if (graderScores is null)
            {
                throw new ArgumentNullException(nameof(graderScores));
            }

            Score = finalScore;
            GraderScores = graderScores;
        }
    }
}
