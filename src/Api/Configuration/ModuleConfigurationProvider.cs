﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AudioGrader.Common.Interfaces;

namespace AudioGrader.Api.Configuration
{
    public class ModuleConfigurationProvider : IModuleConfigurationProvider
    {
        static readonly DirectoryInfo ConfigurationDirectory = new DirectoryInfo(
            Path.Combine(
                Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().GetName().CodeBase ?? ".")?.LocalPath) ?? ".", 
                "Configuration")
            );

        public TConfigModel GetConfiguration<TConfigModel>() where TConfigModel : class, IModuleConfiguration<TConfigModel>, new()
        {
            TConfigModel defaultConfig = new TConfigModel();

            if (!ConfigurationDirectory.Exists)
                ConfigurationDirectory.Create();

            FileInfo configFile = ConfigurationDirectory.EnumerateFiles().FirstOrDefault(x => Path.GetFileNameWithoutExtension(x.FullName) == defaultConfig.ModuleType.Name);
            if (configFile is null)
            {
                defaultConfig.Save(new Uri(Path.Combine(ConfigurationDirectory.FullName,
                    $"{defaultConfig.ModuleType.Name}{(defaultConfig.Extension is null ? string.Empty : ".")}{defaultConfig.Extension ?? string.Empty}")));

                return defaultConfig;
            }

            return defaultConfig.Load(new Uri(configFile.FullName)) ?? defaultConfig;
        }
    }
}
