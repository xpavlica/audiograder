﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using AudioGrader.Configuration;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AudioGrader.Api.Configuration
{
    [ModelBinder(BinderType = typeof(ApiConfigurationBinder))]
    public class ApiConfiguration : JsonModuleConfigurationBase<ApiConfiguration>
    {
        public override Type ModuleType => typeof(ApiConfiguration);

        /// <summary>
        /// The weights each of the grading modules have on the final grade. The key is the grading module's type name (may or may not be fully qualified).
        /// </summary>
        [JsonProperty]
        public Dictionary<string, float> GraderWeights { get; private set; } = new Dictionary<string, float>();

        /// <summary>
        /// The order in which the audio decoding modules are tried on the given file. The values are the grading modules' type names (may or may not be fully qualified).
        /// </summary>
        [JsonProperty]
        public List<string> Decoders { get; private set; } = new List<string>();

        /// <summary>
        /// The audio preprocessor modules (in the order of execution) which are applied to the given audio stream before grading. The values are the preprocessor modules' type names (may or may not be fully qualified).
        /// </summary>
        [JsonProperty]
        public List<string> Preprocessors { get; private set; } = new List<string>();

        /// <summary>
        /// The time after which a rating request times out and the client is returned an operation progress object instead.
        /// </summary>
        [JsonProperty]
        public TimeSpan Timeout { get; set; } = TimeSpan.FromSeconds(60);

        /// <summary>
        /// An interval in which the grader result cache is cleaned of old records.
        /// </summary>
        [JsonProperty]
        public TimeSpan ResultCacheCleanupInterval { get; set; } = TimeSpan.FromMinutes(10);

        /// <summary>
        /// An interval after which the grader result record is marked for deletion.
        /// </summary>
        [JsonProperty]
        public TimeSpan ResultCacheCleanupAge { get; set; } = TimeSpan.FromHours(1);
    }
}
