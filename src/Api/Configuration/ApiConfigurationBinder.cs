﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace AudioGrader.Api.Configuration
{
    public class ApiConfigurationBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var valueResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (valueResult == ValueProviderResult.None)
                return Task.CompletedTask;

            bindingContext.ModelState.SetModelValue(bindingContext.ModelName, valueResult);

            var value = valueResult.FirstValue;

            if (string.IsNullOrEmpty(value))
                return Task.CompletedTask;

            ApiConfiguration configuration = JsonConvert.DeserializeObject<ApiConfiguration>(value);
            bindingContext.Result = ModelBindingResult.Success(configuration);

            return Task.CompletedTask;
        }
    }
}
