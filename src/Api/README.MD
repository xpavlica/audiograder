﻿# Grading API Server
An ASP.NET Core web application for grading audio data by computing a weighted score from grader plugins.

Make sure you have at least the April 2020 update of .NET Core 3.1 CLR to avoid crashes related to unloadable assemblies.

## Setup
0. (*Non-Windows systems only*) Execute this command in the root directory:
    ```
    dotnet sln remove misc/Test.Forms/Test.Forms.csproj && \
    rm -rf misc/Test.Forms && \
    dotnet restore
    ```
1. Build or publish the project (either in Visual Studio or in a terminal by invoking `dotnet build ` in this directory).
2. Edit the `Configuration/ApiConfiguration.json` according to your preferences.
3. Add the desired modules to their respective folders in the `Modules` directory.
4. Start the server by executing `AudioGrader.Api.exe` (only on Windows systems) or by invoking `dotnet AudioGrader.Api.dll`.

The [FFmpeg](https://ffmpeg.org) binaries are bundled with the project for convenience's sake. Their code is not a part of this project and is subject to either the GPL or LGPL license.
## REST Endpoints
The application runs on ports 5000 and 5001 by default, one is with SSL and one without. To change the ports, follow the instructions [here](https://stackoverflow.com/a/37365382).

| HTTP Method | URL            | Query Parameters													| Description                                                                                                                                          |
|-------------|----------------|-------------------------------------------------------------------	|------------------------------------------------------------------------------------------------------------------------------------------------------|
| POST        | /grade         | *configuration* \- JSON configuration of the API call				| Grades the binary audio data in the HTTP request body\.                                                                                              |
| POST        | /grade/form    | *configuration* \- JSON configuration of the API call				| Grades the audio data in the `form-data` field of the HTTP request\.                                                                                  |
| GET         | /grade         | *guid* \- The job GUID												| Returns the given job's status, and, if finished, the grading result\.                                                                               |
| GET         | /configuration | *module \(Optional\)*												| If *module* is set to a correct value, returns the configuration for the given module\. If not set, returns the global API configuration\. |
| GET         | /modules       |																	| Lists all available modules\.                                                                                                                        |

## Modules
A module is a collection of plugins inside a single assembly. For infromation on how to build and add new plugins, see `Modules/MODULES.MD`.

## Configuration
All configuration files are in the `Modules` directory. If they are needed during runtime but not found, a default one is generated in this directory.

### API Configuration
We can set which plugins are used during the grading process. The plugins can be referenced by either their type name (e.g. `class MyGrader : IGrader {...}` = `MyGrader` in the config)
or their fully qualified name (e.g. `AudioGrader.MyModule.MyGrader`).

* **Decoders** - a list of decoding plugins executed in the given order until a compatible one is found or no more are available
* **Preprocessors** - a list of signal preprocessors chained together in the given order and applied to the input audio before passing it to the graders
* **GraderWeights** - a dictionary of *Grader plugin - Weight* pairs used for the calculation of the final grade
* **Timeout** - a time span after which an incomplete job is returned if the grading process takes too long
* **ResultCacheCleanupInterval** - a time span denoting the frequency of job cache being scanned for stale jobs 
* **ResultCacheCleanupAge** - a time span after which a job result is removed from the cache used by the `GET /grade` endpoint
### Module Configuration
The modules have a too many options as to be described here. See the in-code documentation in the respective config classes.

## Issues
* **The web server does not work (*Windows*)** - Allow the application to use the configured ports or run as admin.
* **The web server crashes on start** - Update .NET Core 3.1 to at least the April 2020 version.
* **Requests get permanently stuck on `State = 2` (decoding)** - Make sure both `ffmpeg` and `ffprobe` binaries for your platform are in the `FFMPEG/bin` folder and you have execute permissions on them.
* **Other issues** - Consult the file `log[the date of the issue].txt` in the root directory.