using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using AudioGrader.Common.Interfaces;
using AudioGrader.Api.Configuration;
using AudioGrader.Api.Modules;
using AudioGrader.Api;
using Serilog;
using AudioGrader.Configuration;

namespace AudioGrader
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(x => x.SerializerSettings.ContractResolver = new JsonContractResolver());

            services.AddSingleton(Log.Logger);
            services.AddSingleton<IModuleConfigurationProvider>(new ModuleConfigurationProvider());
            services.AddSingleton<IModuleLoader>(x => new ModuleLoader(x));
            services.AddSingleton(x => new GradingResultCache(x.GetRequiredService<IModuleConfigurationProvider>()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseSerilogRequestLogging();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.ApplicationServices.GetService<IModuleLoader>(); // Warm up the module cache
        }
    }
}
