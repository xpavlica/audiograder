﻿using AudioGrader.Api.Configuration;
using AudioGrader.Api.Models;
using AudioGrader.Common.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AudioGrader.Api
{
    public class GradingResultCache : ConcurrentDictionary<Guid, GradingResult>
    {
#pragma warning disable IDE0052
        readonly Timer timer; //Prevent the GC from collecting the timer
#pragma warning restore IDE0052

        readonly TimeSpan resultCacheCleanupInterval;
        readonly TimeSpan resultCacheCleanupAge;

        public GradingResultCache(IModuleConfigurationProvider configurationProvider) : base() 
        {
            var config = configurationProvider.GetConfiguration<ApiConfiguration>();
            resultCacheCleanupInterval = config.ResultCacheCleanupInterval;
            resultCacheCleanupAge = config.ResultCacheCleanupAge;

            timer = new Timer(OnTimer, null, TimeSpan.Zero, resultCacheCleanupInterval);
        }

        void OnTimer(object? state)
        {
            var guids = this.Where(x => x.Value.StartTime + resultCacheCleanupAge < DateTime.Now).Select(x => x.Key).ToArray();
            foreach (Guid guid in guids)
                TryRemove(guid, out _);
        }
    }
}
