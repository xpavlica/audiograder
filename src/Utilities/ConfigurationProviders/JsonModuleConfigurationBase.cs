﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;
using AudioGrader.Common.Interfaces;
using Newtonsoft.Json;

namespace AudioGrader.Configuration
{
    /// <summary>
    /// A module configuration saved in a JSON file. All properties to be saved must be decorated with the <see cref="JsonPropertyAttribute"/>.
    /// </summary>
    public abstract class JsonModuleConfigurationBase<TConfigModel> : IModuleConfiguration<TConfigModel>
        where TConfigModel : JsonModuleConfigurationBase<TConfigModel>, new()
    {
        public abstract Type ModuleType { get; }

        public string Extension => "json";

        public TConfigModel? Load([NotNull]Uri uri)
        {
            if (uri is null)
            {
                throw new ArgumentNullException(nameof(uri));
            }

            if (!File.Exists(uri.LocalPath))
                return null;

            try
            {
                using JsonTextReader file = new JsonTextReader(File.OpenText(uri.LocalPath));
                JsonSerializer serializer = new JsonSerializer();
                return serializer.Deserialize<TConfigModel>(file);
            }
            catch (UnauthorizedAccessException)
            {
                return null;
            }
        }

        public bool Save([NotNull]Uri uri)
        {
            if (uri is null)
            {
                throw new ArgumentNullException(nameof(uri));
            }

            if (!uri.IsFile)
                return false;

            try
            {
                using JsonTextWriter writer = new JsonTextWriter(new StreamWriter(File.Open(uri.LocalPath, FileMode.Create)));
                JsonSerializer serializer = new JsonSerializer();
                serializer.Formatting = Formatting.Indented;
                serializer.ContractResolver = new JsonContractResolver();
                serializer.Serialize(writer, this);
                return true;
            }
            catch(UnauthorizedAccessException)
            {
                return false;
            }
        }
    }
}
