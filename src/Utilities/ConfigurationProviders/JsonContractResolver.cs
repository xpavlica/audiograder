﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AudioGrader.Configuration
{
    public class JsonContractResolver : DefaultContractResolver
    {
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            return base.CreateProperties(type, memberSerialization).Where(x => x.AttributeProvider?.GetAttributes(true).Any(y => y is JsonPropertyAttribute) ?? false).ToList();
        }
    }
}
