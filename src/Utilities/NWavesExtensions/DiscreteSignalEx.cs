﻿using AudioGrader.Common.Models;
using NWaves.Signals;
using NWaves.Transforms;
using NWaves.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AudioGrader.Utilities.NWavesExtensions
{
    public static class DiscreteSignalEx
    {
        /// <summary>
        /// Downmixes the collection of signals with the same sampling rate into a singular signal by averaging. The signals can be of variable length.
        /// Note that mixing otherwise identical out-of-phase signals will result in silence.
        /// </summary>
        /// <param name="signals"></param>
        /// <returns></returns>
        public static DiscreteSignal DownmixToMono(this IEnumerable<DiscreteSignal> signals)
        {
            if (!signals.Any())
                throw new ArgumentException("The collection is empty.", nameof(signals));

            int samplingRate = signals.First().SamplingRate;

            if (signals.Any(x => x.SamplingRate != samplingRate))
                throw new ArgumentException("The sampling rates of given signals do not match.", nameof(signals));

            float[] buffer = new float[signals.Max(x => x.Length)];

            // The most common use is to downmix stereo audio - this optimization cuts the copying worldload to about half (performance-wise)
            DiscreteSignal longestSignal = signals.FirstOrDefault(x => x.Length == buffer.Length);
            Buffer.BlockCopy(longestSignal.Samples, 0, buffer, 0, sizeof(float) * longestSignal.Length);

            foreach (DiscreteSignal signal in signals.Where(x => x != longestSignal))
            {
                float[] samples = signal.Samples;
                for (int i = 0; i < signal.Length; i++)
                    buffer[i] += samples[i];
            }

            int[] lengths = signals.Select(x => x.Length).OrderBy(x => x).ToArray();
            int signalCount = signals.Count();
            for (int i = 0, mixedSignals = signalCount; i < buffer.Length; i++)
            {
                if (i == lengths[signalCount - mixedSignals])
                {
                    for (; i == lengths[signalCount - mixedSignals]; mixedSignals--)
                        ;
                }
                buffer[i] /= mixedSignals;
            }

            return new DiscreteSignal(samplingRate, buffer);
        }

        /// <summary>
        /// Converts the samples into an array of doubles.
        /// </summary>
        /// <param name="signal"></param>
        /// <returns></returns>
        public static double[] ToDoubleArray(this DiscreteSignal signal) => signal.Samples.Select(x => (double)x).ToArray();

        /// <summary>
        /// Returns an a-weighted RMS value (mean perceived loudness) of the given signal.
        /// </summary>
        /// <param name="fftSize">The number of FFT bins</param>
        /// <returns></returns>
        public static double Loudness(this DiscreteSignal signal, int fftSize = 1024)
        {
            Guard.AgainstNotPowerOfTwo(fftSize, nameof(fftSize));

            Fft fft = new Fft(fftSize);
            DiscreteSignal spectrum = fft.PowerSpectrum(signal);
            for (int i = 1; i < spectrum.Length; i++)
            {
                double frequency = i * (signal.SamplingRate / fftSize / 2d);
                double aWeight = Scale.LoudnessWeighting(frequency);
                spectrum.Samples[i] *= (float)aWeight;
            }
            // Using Parseval's theorem to calculate the RMS without doing an inverse FFT
            return spectrum.Rms() / Math.Sqrt(spectrum.Length);
        }

        /// <summary>
        /// Converts the <see cref="DiscreteSignal"/> to a Data Transfer Object.
        /// </summary>
        /// <param name="signal"></param>
        public static DiscreteSignalDto ToDto(this DiscreteSignal signal) => new DiscreteSignalDto(signal.SamplingRate, signal.Samples, false);

        /// <summary>
        /// Converts the Data Tranfer Object to a <see cref="DiscreteSignal"/>.
        /// </summary>
        /// <param name="dto"></param>
        public static DiscreteSignal ToNWavesDiscreteSignal(this DiscreteSignalDto dto) => new DiscreteSignal(dto.SampleRate, dto.Samples, false);
    }
}
