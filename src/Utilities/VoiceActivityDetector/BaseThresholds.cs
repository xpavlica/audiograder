﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AudioGrader.Utilities.VoiceActivityDetector
{
    public struct BaseThresholds
    {
        [JsonProperty]
        public float energy;
        [JsonProperty]
        public int dominantFrequencyComponent;
        [JsonProperty]
        public float spectralFlatness;

        public static BaseThresholds Default => new BaseThresholds()
        {
            energy = 480,
            dominantFrequencyComponent = 180,
            spectralFlatness = 2.7f,
        };

        public override bool Equals(object obj)
        {
            if (!(obj is BaseThresholds other))
                return false;
            return energy == other.energy && dominantFrequencyComponent == other.dominantFrequencyComponent && spectralFlatness == other.spectralFlatness;
        }

        public override int GetHashCode() => (int)energy ^ dominantFrequencyComponent ^ (int)spectralFlatness;

        public static bool operator ==(BaseThresholds left, BaseThresholds right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(BaseThresholds left, BaseThresholds right)
        {
            return !(left == right);
        }
    }
}
