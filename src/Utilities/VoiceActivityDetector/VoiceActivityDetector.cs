﻿using System;
using System.Linq;
using NWaves.Features;
using NWaves.Signals;
using NWaves.Transforms;
using Linq.Extras;
using System.Collections.Generic;

namespace AudioGrader.Utilities.VoiceActivityDetector
{
    /// <summary>
    /// An implementation of the paper "A SIMPLE BUT EFFICIENT REAL-TIME VOICE ACTIVITY DETECTION ALGORITHM" (DOI 10.1.1.176.6740)
    /// </summary>
    public static class VoiceActivityDetector
    {
        const float FrameSize = 0.01f; // Frame size in seconds
        const int FftSize = 1024;

        /// <summary>
        /// Returns an array of boolean values indicating voice activity being detected on the given sample.
        /// </summary>
        /// <param name="signal">The normalized audio signal</param>
        public static bool[] Detect(DiscreteSignal signal, BaseThresholds? baseThresholds = null)
        {
            baseThresholds ??= BaseThresholds.Default;

            int frameSampleCount = (int)(signal.SamplingRate * FrameSize);
            int frameCount = (int)Math.Ceiling(signal.Length / (float)frameSampleCount);
            float[] frameSamples = new float[frameSampleCount];
            bool[] voicePresent = new bool[frameCount];

            var lastSample = signal.Samples.Length - 1;

            float minEnergy = float.MaxValue;
            int minDominantComponent = int.MaxValue;
            float minFlatness = float.MaxValue;
            int silentFrameCount = 0;
            int minCalculationFrames = 30;

            float signalRms = (signal * (1 << 15)).Rms();

            Fft fft = new Fft(FftSize);

            for (int sample = 0, frameIndex = 0; sample <= lastSample; sample += frameSampleCount, frameIndex++)
            {
                if (sample + frameSampleCount > signal.Length)
                    Array.Clear(frameSamples, 0, frameSamples.Length);

                Buffer.BlockCopy(signal.Samples, sample * sizeof(float), frameSamples, 0, Math.Min(frameSampleCount, signal.Length - sample) * sizeof(float));

                DiscreteSignal frame = new DiscreteSignal(signal.SamplingRate, frameSamples);
                frame.Amplify(1 << 15); // Convert to 16-bit value range, normalized values break the min energy computations (log is always negative)
                
                float rms = frame.Rms(); // The root mean square yields more accurate results than energy

                // Some recording equipment starts emitting significantly more noise after first period of activity, thus breaking the minimum energy computations.
                // To prevent this, very low-energy frames are preemptively discarded.
                if (rms <= 0.1f * signalRms)
                {
                    silentFrameCount++;
                    voicePresent[frameIndex] = false;
                    continue;
                }

                DiscreteSignal spectrum = fft.MagnitudeSpectrum(frame);
                int dominantComponent = spectrum.Samples.WithIndex().MaxBy(x => x.Item).Index * (signal.SamplingRate / FftSize / 2); // Converted to Hz

                // The flatness in the paper is in dB (even though not explicitly stated, can be inferred from the graphs and the recommended defaults).
                float flatness = 10 * (float)Math.Abs(Math.Log10(Spectral.Flatness(spectrum.Samples)));

                if (minCalculationFrames > 0)
                {
                    minCalculationFrames--;
                    minEnergy = Math.Min(minEnergy, rms);
                    minDominantComponent = Math.Min(minDominantComponent, dominantComponent);
                    minFlatness = Math.Min(minFlatness, flatness);
                }

                float energyThreshold = (float)(baseThresholds.Value.energy * Math.Log10(minEnergy));

                bool voiceDetected = (rms - minEnergy) >= energyThreshold ||
                                        (dominantComponent - minDominantComponent) >= baseThresholds.Value.dominantFrequencyComponent ||
                                        (flatness - minFlatness) >= baseThresholds.Value.spectralFlatness;

                voicePresent[frameIndex] = voiceDetected;

                if (!voiceDetected)
                {
                    silentFrameCount++;
                    minEnergy = ((silentFrameCount * minEnergy) + rms) / (silentFrameCount + 1);
                }
            }

            return voicePresent
                .GroupUntilChanged()
                .Select(IgnoreSilenceRun)
                .SelectMany(x => x)
                .GroupUntilChanged()
                .Select(IgnoreSpeechRun)
                .Select(x => Enumerable.Repeat(x.First(), x.Count() * frameSampleCount))
                .SelectMany(x => x)
                .ToArray();
        }

        static IEnumerable<bool> IgnoreSilenceRun(IGrouping<bool, bool> group)
        {
            int count = group.Count();
            if (!group.Key && count < 10)
                return Enumerable.Repeat(true, count); // Ignore small silence runs
            return group;
        }

        static IEnumerable<bool> IgnoreSpeechRun(IGrouping<bool, bool> group)
        {
            int count = group.Count();
            if (group.Key && count < 5)
                return Enumerable.Repeat(false, count); // Ignore small speech runs
            return group;
        }
    }
}
