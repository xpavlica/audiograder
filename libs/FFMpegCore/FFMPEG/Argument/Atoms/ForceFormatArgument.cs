﻿using FFMpegCore.FFMPEG.Enums;

namespace FFMpegCore.FFMPEG.Argument
{
    /// <summary>
    /// Represents force format parameter
    /// </summary>
    public class ForceFormatArgument : Argument<Format>
    {
        public ForceFormatArgument() { }

        public ForceFormatArgument(Format value) : base(value) { }

        /// <inheritdoc/>
        public override string GetStringValue()
        {
            return $"-f {Value.ToString().ToLower()}";
        }
    }
}
