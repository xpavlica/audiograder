﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FFMpegCore.FFMPEG.Argument
{
    /// <summary>
    /// Enable bitexact mode for (de)muxer and (de/en)coder
    /// </summary>
    public class BitExactArgument : Argument
    {
        public override string GetStringValue()
        {
            return "-bitexact";
        }
    }
}
