﻿namespace FFMpegCore.FFMPEG.Enums
{
    public enum VideoCodec
    {
        LibX264,
        LibVpx,
        LibTheora,
        Png,
        MpegTs
    }

    public enum AudioCodec
    {
        Aac,
        LibVorbis,
        LibFdk_Aac,
        Ac3,
        Eac3,
        LibMp3Lame,
        Pcm_S16LE
    }

    public enum Format
    {
        Mp4,
        Avi,
        Mkv,
        Ogv,
        Vpx,
        Mpeg,
        Ogg,
        Aac,
        Mp3,
        Wav
    }

    public enum Filter
    {
        H264_Mp4ToAnnexB,
        Aac_AdtstoAsc
    }

    public enum Channel
    {
        Audio,
        Video,
        Both
    }
}