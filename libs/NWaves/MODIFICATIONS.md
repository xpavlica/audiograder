﻿* Made WaveFile.SupportedBitDepths static readonly
* Added an option to the WaveFile contructor to not close the wav file stream. 