﻿using BenchmarkDotNet.Running;
using System;
using System.Threading.Tasks;

namespace Benchmarks
{
    class Program
    {
        static void Main(string[] args)
        {
            // You need to supply the FFmpeg binaries for the given platform and change the paths to the FFmpeg executable and test data in the Globals.cs file.
            /*BenchmarkRunner.Run<Decoders>();
            BenchmarkRunner.Run<Preprocessors>();*/
            //BenchmarkRunner.Run<Graders>();
            BenchmarkRunner.Run<GradingEndpoint>();
        }
    }
}
