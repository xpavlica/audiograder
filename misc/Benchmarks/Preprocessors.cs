﻿using AudioGrader.Common.Models;
using BenchmarkDotNet.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Benchmarks
{
    public class Preprocessors
    {
        readonly IEnumerable<DiscreteSignalDto>? testFile;

        public Preprocessors()
        {
            using Stream wav = File.OpenRead(Globals.DialTonePath);
            testFile = new AudioGrader.Decoders.WavDecoder.WavDecoder().ToChannelSignals(wav).Result;
            if (testFile is null)
                throw new InvalidDataException();
        }

        [Benchmark]
        public async Task RingToneCutter()
        {
            await new AudioGrader.Preprocessors.RingingToneCutter.RingingToneCutter(new DummyConfigurationProvider()).Apply(testFile.First());
        }
    }
}
