﻿using AudioGrader.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Benchmarks
{
    class DummyConfigurationProvider : IModuleConfigurationProvider
    {
        public TConfigModel GetConfiguration<TConfigModel>() where TConfigModel : class, IModuleConfiguration<TConfigModel>, new()
        {
            return new TConfigModel();
        }
    }
}
