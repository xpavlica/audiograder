﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Benchmarks
{
    static class ExtensionMethods
    {
        public static void AddRange<T, U>(this IDictionary<T, U> dict, IEnumerable<KeyValuePair<T, U>> values) where T : notnull
        {
            foreach (var val in values)
                dict.Add(val);
        }
    }
}
