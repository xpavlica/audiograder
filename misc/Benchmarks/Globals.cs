﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Benchmarks
{
    public static class Globals
    {
        public static readonly string WavFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "the_art_of_war.wav");
        public static readonly string OggFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "the_art_of_war.ogg");
        public static readonly string ShortFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "the_art_of_war_6min.wav");
        public static readonly string DialTonePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "dialtone_1h.wav");
        public static readonly string FFmpegExecutableDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
    }
}
