﻿using AudioGrader.Common.Interfaces;
using AudioGrader.Decoders.FFmpegDecoder;
using AudioGrader.Decoders.WavDecoder;
using AudioGrader.Graders.Clipping.ClippingHistogram;
using AudioGrader.Graders.Cutoff.VoiceCutoffByRms;
using AudioGrader.Graders.Noise.AverageSNRFromVAD;
using AudioGrader.Graders.Silence.SilenceRunsFromVAD;
using AudioGrader.Graders.Volume.AverageVolumeJump;
using AudioGrader.Graders.Volume.VolumeHistogram;
using AudioGrader.Preprocessors.RingingToneCutter;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace Benchmarks
{
    class DummyModuleLoader : AudioGrader.Api.Modules.IModuleLoader
    {
        IServiceProvider serviceProvider = new DefaultServiceProviderFactory().CreateBuilder(new ServiceCollection())
                .AddSingleton(Mock.Of<Serilog.ILogger>())
                .AddSingleton<IModuleConfigurationProvider>(new DummyConfigurationProvider()).BuildServiceProvider();

        static readonly Dictionary<string, Type> types = new[]
        {
            typeof(WavDecoder),
            typeof(FFmpegDecoder),
            typeof(ClippingHistogram),
            typeof(VoiceCutoffByRms),
            typeof(AverageSNRFromVAD),
            typeof(SilenceRunsFromVAD),
            typeof(AverageVolumeJump),
            typeof(VolumeHistogram),
            typeof(RingingToneCutter)
        }.ToDictionary(x => x.Name, x => x);
        
        public string GetModuleAssemblyFilePath(string moduleName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> ListModules()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> ListModules<T>()
        {
            throw new NotImplementedException();
        }

        public object LoadModule(string typeName)
        {
            return ActivatorUtilities.CreateInstance(serviceProvider, types[typeName]);
        }

        public T? LoadModule<T>(string typeName) where T : class
        {
            return (T)LoadModule(typeName);
        }
    }
}
