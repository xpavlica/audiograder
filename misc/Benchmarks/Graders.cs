﻿using AudioGrader.Common.Interfaces;
using AudioGrader.Common.Models;
using AudioGrader.Decoders.WavDecoder;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using AudioGrader.Graders;
using AudioGrader.Graders.Clipping.ClippingHistogram;
using BenchmarkDotNet.Attributes;
using AudioGrader.Graders.Cutoff.VoiceCutoffByRms;
using AudioGrader.Graders.Noise.AverageSNRFromVAD;
using AudioGrader.Graders.Silence.SilenceRunsFromVAD;
using AudioGrader.Graders.Volume.AverageVolumeJump;
using AudioGrader.Graders.Volume.VolumeHistogram;
using Microsoft.Diagnostics.Tracing.Parsers.AspNet;

namespace Benchmarks
{
    public class Graders
    {
        readonly IEnumerable<DiscreteSignalDto>? testFile;
        IServiceProvider serviceProvider;

        public Graders()
        {
            using Stream wav = File.OpenRead(Globals.WavFilePath);
            testFile = new WavDecoder().ToChannelSignals(wav).Result;
            if (testFile is null)
                throw new InvalidDataException();
            var factory = new DefaultServiceProviderFactory();
            serviceProvider = factory.CreateBuilder(new ServiceCollection())
                .AddSingleton(Mock.Of<Serilog.ILogger>())
                .AddSingleton<IModuleConfigurationProvider>(new DummyConfigurationProvider()).BuildServiceProvider();
        }

        async Task Run<T>() where T : IGrader
        {
            await ActivatorUtilities.CreateInstance<T>(serviceProvider).Grade(testFile!).ConfigureAwait(false);
        }

        [Benchmark]
        public async Task ClippingHistogram() => await Run<ClippingHistogram>();

        [Benchmark]
        public async Task VoiceCutoffByRms() => await Run<VoiceCutoffByRms>();

        [Benchmark]
        public async Task AverageSNRFromVAD() => await Run<AverageSNRFromVAD>();

        [Benchmark]
        public async Task SilenceRunsFromVAD() => await Run<SilenceRunsFromVAD>();

        [Benchmark]
        public async Task AverageVolumeJump() => await Run<AverageVolumeJump>();

        [Benchmark]
        public async Task VolumeHistogram() => await Run<VolumeHistogram>();
    }
}
