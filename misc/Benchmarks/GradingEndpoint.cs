﻿using AudioGrader.Api.Controllers;
using AudioGrader.Common.Models;
using AudioGrader.Decoders.WavDecoder;
using BenchmarkDotNet.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Benchmarks
{
    public class GradingEndpoint
    {
        readonly GradeController controller;
        readonly AudioGrader.Common.Interfaces.IModuleConfigurationProvider configurationProvider = new DummyConfigurationProvider();
        readonly AudioGrader.Api.GradingResultCache cache;
        readonly MemoryStream testFile = new MemoryStream();
        readonly AudioGrader.Api.Configuration.ApiConfiguration apiConfiguration;

        [Params(1, 2, 3, 4, 5, 6)]
        public int ConcurrentTaskCount { get; set; }

        public GradingEndpoint()
        {
            using Stream wav = File.OpenRead(Globals.ShortFilePath);
            wav.CopyTo(testFile);
            controller = new GradeController(new DummyConfigurationProvider(), new DummyModuleLoader(), cache);
            cache = new AudioGrader.Api.GradingResultCache(configurationProvider);

            apiConfiguration = new AudioGrader.Api.Configuration.ApiConfiguration();
            apiConfiguration.Decoders.Add("WavDecoder");
            apiConfiguration.Preprocessors.Add("RingingToneCutter");
            apiConfiguration.GraderWeights.AddRange(new Dictionary<string, float>()
            {
                ["ClippingHistogram"] = 1,
                ["VoiceCutoffByRms"] = 1,
                ["AverageSNRFromVAD"] = 1,
                ["SilenceRunsFromVAD"] = 1,
                ["AverageVolumeJump"] = 1,
                ["VolumeHistogram"] = 1
            });
        }

        [IterationSetup]
        public void Setup()
        {
            testFile.Position = 0;
        }

        [Benchmark]
        public async Task GradeRequest()
        {
            var tasks = Enumerable.Range(0, ConcurrentTaskCount).Select(x => controller.Grade(testFile, new AudioGrader.Api.Models.GradingResult(), apiConfiguration)).ToArray();
            await Task.WhenAll(tasks).ConfigureAwait(false);
        }

    }
}
