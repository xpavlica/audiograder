﻿using AudioGrader.Decoders.FFmpegDecoder;
using AudioGrader.Decoders.WavDecoder;
using BenchmarkDotNet.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Benchmarks
{
    public class Decoders
    {
        readonly MemoryStream wavStream = new MemoryStream();
        readonly MemoryStream oggStream = new MemoryStream();

        public Decoders()
        {
            FFMpegCore.FFMPEG.FFMpegOptions.Options.RootDirectory = Globals.FFmpegExecutableDirectory;

            using Stream wav = File.OpenRead(Globals.WavFilePath);
            wav.CopyTo(wavStream);
            using Stream ogg = File.OpenRead(Globals.OggFilePath);
            ogg.CopyTo(oggStream);
        }

        [IterationSetup]
        public void Setup()
        {
            wavStream.Position = 0;
            oggStream.Position = 0;
        }

        [Benchmark]
        public async Task WavDecoder()
        {
            await new WavDecoder().ToChannelSignals(wavStream);
        }

        [Benchmark]
        public async Task FFmpegDecoder_Wav()
        {
            await new FFmpegDecoder().ToChannelSignals(wavStream);
        }

        [Benchmark]
        public async Task FFmpegDecoder_Ogg()
        {
            await new FFmpegDecoder().ToChannelSignals(wavStream);
        }

    }
}
