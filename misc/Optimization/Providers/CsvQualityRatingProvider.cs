﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace Optimization
{
    public class CsvQualityRatingProvider : IQualityRatingProvider
    {
        readonly Dictionary<string, QualityRating> ratings = new Dictionary<string, QualityRating>();

        public CsvQualityRatingProvider(string csvPath, uint idRowIndex, Func<IList<string>, QualityRating> ratingFactory, Action<IReaderConfiguration>? configuration = null)
        {
            if (!File.Exists(csvPath))
                throw new ArgumentException("The csv file does not exist.", nameof(csvPath));

            using var reader = new StreamReader(csvPath);
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            configuration?.Invoke(csv.Configuration);

            if (csv.Configuration.HasHeaderRecord)
                csv.Read();

            List<string> records = new List<string>();
            while (csv.Read())
            {
                records.Clear();
                for (int i = 0; csv.TryGetField(i, out string cell); ++i)
                    records.Add(cell);

                if (idRowIndex >= records.Count)
                    throw new ArgumentException("The row index is out of range.", nameof(idRowIndex));

                ratings[records[(int)idRowIndex]] = ratingFactory(records);
            }
        }

        public QualityRating? GetRating(string audioId) => ratings.TryGetValue(audioId, out var rating) ? rating : (QualityRating?)null;
    }
}
