﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper;
using CsvHelper.Configuration;

namespace Optimization
{
    public class CsvAudioDataProvider : ILazyAudioDataProvider
    {
        public string AudioDataPath { get; private set; }
        public string CsvPath { get; private set; }
        public string Extension { get; private set; }
        public int FileNameColumnIndex { get; private set; }

        Action<IReaderConfiguration>? Configuration { get; set; }

        readonly List<string> filenames = new List<string>();

        public CsvAudioDataProvider(string audioDataPath, string csvPath, int filenameColumnIndex, string extension = "wav", Action<IReaderConfiguration>? configuration = null)
        {
            if (!Directory.Exists(audioDataPath))
                throw new ArgumentException("The audio data path does not exist.", nameof(audioDataPath));
            if (!File.Exists(csvPath))
                throw new ArgumentException("The csv file does not exist.", nameof(csvPath));

            AudioDataPath = audioDataPath;
            CsvPath = csvPath;
            FileNameColumnIndex = filenameColumnIndex;
            Extension = extension;
            Configuration = configuration;
        }

        void ParseCsv()
        {
            filenames.Clear();

            using var reader = new StreamReader(CsvPath);
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            Configuration?.Invoke(csv.Configuration);

            if (csv.Configuration.HasHeaderRecord)
                reader.ReadLine();
            while (csv.Read())
            {
                filenames.Add(csv.GetField(FileNameColumnIndex));
            }
        }

        Func<Stream?> GetAudioStream(string name)
        {
            string path = Path.Combine(AudioDataPath, $"{name}.{Extension}");

            return () =>
            {
                if (!File.Exists(path))
                    return null;
                return File.OpenRead(path);
            };
        }

        public IDictionary<string, Func<Stream?>> GetDataLazy()
        {
            ParseCsv();
            return filenames.ToDictionary(x => x, x => GetAudioStream(x));
        }
    }
}
