﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Optimization.Providers
{
    public class SupportCsvQualityRatingProvider : CsvQualityRatingProvider
    {
        static readonly IFormatProvider FormatProvider = new CultureInfo("cs-CZ");

        static readonly Func<IList<string>, QualityRating> RatingFactory = x => new QualityRating() 
        { 
            overall = float.Parse(x[0], FormatProvider),
            noise = float.Parse(x[1], FormatProvider),
            suddenEnd = float.Parse(x[2], FormatProvider),
            clipping = float.Parse(x[3], FormatProvider),
            volumeVariation = float.Parse(x[4], FormatProvider),
            silenceRuns = float.Parse(x[5], FormatProvider)
        };

        public static readonly Action<IReaderConfiguration> ConfigurationAction = x =>
        {
            x.HasHeaderRecord = true;
            x.Delimiter = ";";
        };

        public SupportCsvQualityRatingProvider(string csvPath) : base(csvPath, 6, RatingFactory, ConfigurationAction) { }
    }
}
