﻿using AudioGrader.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimization
{
    class ConfigurationProvider : IModuleConfigurationProvider
    {
        public IDictionary<Type, object> Overrides { get; } = new Dictionary<Type, object>();

        public TConfigModel GetConfiguration<TConfigModel>() where TConfigModel : class, IModuleConfiguration<TConfigModel>, new()
        {
            if (Overrides.TryGetValue(typeof(TConfigModel), out var config) && config != null && config is TConfigModel)
                return (TConfigModel)config;

            return new TConfigModel();
        }
    }
}
