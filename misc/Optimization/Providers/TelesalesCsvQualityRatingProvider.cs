﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimization
{
    public class TelesalesCsvQualityRatingProvider : CsvQualityRatingProvider
    {
        static readonly Func<IList<string>, QualityRating> RatingFactory = x => new QualityRating() { overall = float.Parse(x[1]) };

        public static readonly Action<IReaderConfiguration> ConfigurationAction = x =>
        {
            x.HasHeaderRecord = true;
            x.Delimiter = ";";
        };

        public TelesalesCsvQualityRatingProvider(string csvPath) : base(csvPath, 3, RatingFactory, ConfigurationAction) { }
    }
}
