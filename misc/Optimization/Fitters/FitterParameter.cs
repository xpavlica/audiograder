﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Optimization
{
    public class FitterParameter<T> : IEquatable<FitterParameter<T>>
    {
        static readonly IFormatProvider Format = System.Globalization.CultureInfo.InvariantCulture;

        readonly Expression<Func<T, IConvertible>> selector;
        readonly Func<T, IConvertible> compiled;

        public PropertyInfo Property { get; private set; } 

        public FitterParameter(Expression<Func<T, IConvertible>> selector)
        {
            if (!IsValidExpression(selector))
                throw new ArgumentException("The selector has to be a simple member expression.", nameof(selector));

            this.selector = selector;
            compiled = selector.Compile();
            Property = ExtractPropertyInfo(selector);
        }

        public double GetValue(T instance) => compiled(instance).ToDouble(Format);

        public void SetValue(T instance, double value)
        {
            object converted = Convert.ChangeType(value, Property.PropertyType);
            Property.SetValue(instance, converted, null);
        }

        static PropertyInfo ExtractPropertyInfo(Expression<Func<T, IConvertible>> selector)
        {
            MemberExpression? memberExpression = selector.Body as MemberExpression;

            if (memberExpression is null && selector.Body is UnaryExpression unaryExpression)
            {
                if (unaryExpression.NodeType == ExpressionType.Convert || unaryExpression.NodeType == ExpressionType.ConvertChecked)
                {
                    memberExpression = unaryExpression.Operand as MemberExpression;
                }
            }

            if (memberExpression is null)
                throw new InvalidCastException("The selector cannot be converted to a setter.");

            if (memberExpression.Member.MemberType != MemberTypes.Property)
                throw new NotSupportedException("Only property selectors are supported.");

            return (PropertyInfo)memberExpression.Member;
        }

        public static FitterParameter<T>[] CreateMany(params Expression<Func<T, IConvertible>>[] selectors) => selectors.Select(x => new FitterParameter<T>(x)).ToArray();

        public static implicit operator FitterParameter<T>(Expression<Func<T, IConvertible>> selector) => new FitterParameter<T>(selector);

        static bool IsValidExpression(Expression<Func<T, IConvertible>> selector)
        {
            if (selector.Body is MemberExpression)
                return true;

            if (selector.Body is UnaryExpression unary && (unary.NodeType == ExpressionType.Convert || unary.NodeType == ExpressionType.ConvertChecked))
                return true;

            return false;
        }

        public bool Equals([AllowNull] FitterParameter<T> other)
        {
            if (other is null)
                return false;

            var thisProperty = (PropertyInfo)((MemberExpression)selector.Body).Member;
            var otherProperty = (PropertyInfo)((MemberExpression)selector.Body).Member;
            return thisProperty.Equals(otherProperty);
        }
    }

    public class FitterParameterEqualityComparer<T> : IEqualityComparer<FitterParameter<T>>
    {
        public static FitterParameterEqualityComparer<T> Default { get; } = new FitterParameterEqualityComparer<T>();

        public bool Equals([AllowNull] FitterParameter<T> x, [AllowNull] FitterParameter<T> y)
        {
            if (x is null && y is null)
                return true;
            if (x is null || y is null)
                return false;
            return x.Property.Equals(y.Property);
        }

        public int GetHashCode([DisallowNull] FitterParameter<T> obj)
        {
            return obj.Property.GetHashCode();
        }
    }
}
