﻿using Accord.Math.Optimization.Losses;
using Accord.Statistics.Models.Regression.Fitting;
using Accord.Statistics.Models.Regression.Linear;
using AudioGrader.Common.Interfaces;
using AudioGrader.Common.Models;
using AudioGrader.Decoders.FFmpegDecoder;
using MathNet.Numerics;
using MathNet.Numerics.LinearRegression;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimization
{
    public class GradingWeightFitter
    {
        readonly IQualityRatingProvider qualityRatingProvider;
        readonly IDictionary<string, Func<Stream?>> audioData;
        readonly List<IGrader> graders = new List<IGrader>();
        readonly Dictionary<string, KeyValuePair<Type, GraderResult>[]> graderResults = new Dictionary<string, KeyValuePair<Type, GraderResult>[]>();
        readonly IAudioDecoder audioDecoder;

        public GradingWeightFitter(IQualityRatingProvider qualityRatingProvider, ILazyAudioDataProvider audioDataProvider, IServiceProvider serviceProvider,
            IEnumerable<Type> graders, IAudioDecoder decoder)
        {
            this.qualityRatingProvider = qualityRatingProvider;
            audioDecoder = decoder;

            if (graders.Any(x => !x.GetInterfaces().Contains(typeof(IGrader))))
                throw new ArgumentException($"All graders do not implement the {nameof(IGrader)} interface.", nameof(graders));

            foreach (Type t in graders)
            {
                if (!(ActivatorUtilities.CreateInstance(serviceProvider, t) is IGrader grader))
                    throw new ArgumentException($"The service provider does not satisfy all dependencies of the grader {t.Name}.", nameof(serviceProvider));
                this.graders.Add(grader);
            }

            audioData = audioDataProvider.GetDataLazy();
        }

        async Task GradeAudio(string id)
        {
            if (!audioData.ContainsKey(id))
                return;

            using Stream? audioStream = audioData[id]();
            if (audioStream is null)
                return;

            if (!audioStream.CanSeek || !await audioDecoder.Supports(audioStream))
                return;

            audioStream.Position = 0;
            var signals = await audioDecoder.ToChannelSignals(audioStream);

            if (signals is null)
                return;

            var gradingTasks = graders.Select(x => x.Grade(signals)).ToArray();
            await Task.WhenAll(gradingTasks);

            if (gradingTasks.Any(x => x.Result == null))
                return;

            graderResults.Add(id, graders.Zip(gradingTasks).Select(y => new KeyValuePair<Type, GraderResult>(y.First.GetType(), y.Second.Result ?? new GraderResult(0f))).ToArray());
        }

        async Task GradeAll()
        {
            foreach (var key in audioData.Keys)
                await GradeAudio(key);
        }

        public async Task<IDictionary<Type, double>> Optimize()
        {
            if (!graderResults.Any())
                await GradeAll();

            var samples = graderResults.Select(x => Tuple.Create(x.Value.Select(y => y.Value.Score).ToArray(), qualityRatingProvider.GetRating(x.Key)?.overall ?? 0f));
            double[][] graderData = graderResults.Select(x => x.Value.Select(y => (double)y.Value.Score).ToArray()).ToArray();
            double[] collectedData = graderResults.Select(x => qualityRatingProvider.GetRating(x.Key)?.overall ?? 0d).ToArray();

            var nnls = new NonNegativeLeastSquares()
            {
                MaxIterations = 10000
            };
            MultipleLinearRegression regression = nnls.Learn(graderData, collectedData);

            return regression.Weights.Select((x, index) => new KeyValuePair<Type, double>(graders[index].GetType(), x)).ToDictionary(x => x.Key, x => x.Value);
        }

        public async Task<double> GetMeanError(IDictionary<Type, double> weights)
        {
            if (!graderResults.Any())
                await GradeAll();

            var graderScores = graderResults.ToDictionary(x => x.Key, x => GetGraderScore(x.Key, weights));

            return graderScores.Average(x => Math.Abs(x.Value - qualityRatingProvider.GetRating(x.Key)?.overall ?? 0f));
        }

        double GetGraderScore(string id, IDictionary<Type, double> weights)
        {
            var scores = graderResults[id].ToDictionary(x => x.Key, x => x.Value);
            double weightSum = weights.Where(x => scores.ContainsKey(x.Key)).Sum(x => x.Value);
            return scores.Where(x => weights.ContainsKey(x.Key)).Select(x => weights[x.Key] / weightSum * x.Value.Score).Sum();
        }
    }
}
