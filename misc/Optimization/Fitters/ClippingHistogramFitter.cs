﻿using AudioGrader.Graders.Clipping.ClippingHistogram;
using System;
using System.Collections.Generic;
using System.Text;
using Accord.Math.Optimization;
using AudioGrader.Common.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Optimization.Fitters
{
    public class ClippingHistogramFitter : ModuleFitter<ClippingHistogram, Settings>
    {
        protected override IList<FitterParameter<Settings>> Parameters => FitterParameter<Settings>.CreateMany(
            x => x.BadRatio,
            x => x.BucketCount,
            x => x.ExcellentRatio);

        protected override IEnumerable<FitterConstraint<Settings>> Constraints => new[]
        {
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.BadRatio), x => x, ConstraintType.GreaterThanOrEqualTo, 0.01),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.BadRatio), x => x, ConstraintType.LesserThanOrEqualTo, 5d),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.ExcellentRatio), x => x, ConstraintType.GreaterThanOrEqualTo, 0.01),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.ExcellentRatio), x => x, ConstraintType.LesserThanOrEqualTo, 10),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.BucketCount), x => x, ConstraintType.GreaterThanOrEqualTo, 3),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.BucketCount), x => x, ConstraintType.LesserThanOrEqualTo, 80),
            new FitterConstraint<Settings>(FitterParameter<Settings>.CreateMany(x => x.BadRatio, x => x.ExcellentRatio), x => x[1] - x[0], ConstraintType.GreaterThanOrEqualTo, 0.01)
        };

        protected override double ScalingFactor => 0.01;


        public ClippingHistogramFitter(IQualityRatingProvider qualityRatingProvider, ILazyAudioDataProvider audioDataProvider, IAudioDecoder audioDecoder,
            Action<IServiceCollection>? serviceSetup = null)
            : base(qualityRatingProvider, audioDataProvider, audioDecoder, x => x.clipping , serviceSetup) { }
    }
}
