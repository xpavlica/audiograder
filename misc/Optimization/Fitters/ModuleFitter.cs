﻿using Accord.Math.Optimization;
using AudioGrader.Common.Interfaces;
using AudioGrader.Common.Models;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimization
{
    public abstract class ModuleFitter<TModule, TModuleConfig> 
        where TModule : IGrader 
        where TModuleConfig : class, IModuleConfiguration<TModuleConfig>, new()
    {
        readonly Action<IServiceCollection>? serviceSetup;
        readonly IQualityRatingProvider qualityRatingProvider;
        readonly IDictionary<string, Func<Stream?>> audioData;
        readonly IAudioDecoder audioDecoder;
        readonly Func<QualityRating, float> ratingSelector;

        protected abstract IList<FitterParameter<TModuleConfig>> Parameters { get; }
        protected abstract IEnumerable<FitterConstraint<TModuleConfig>> Constraints { get; }
        IEnumerable<FitterConstraint<TModuleConfig>> scaledConstraints;

        // The optimization algorithm runs in very small iuncrements. This means that it can plateau on integral types or floating point types with a large value ranges.
        /// <summary>
        /// The scaling factor of all parameters.
        /// </summary>
        protected abstract double ScalingFactor { get; }

        protected ModuleFitter(IQualityRatingProvider qualityRatingProvider, ILazyAudioDataProvider audioDataProvider, IAudioDecoder audioDecoder, 
            Func<QualityRating, float> ratingSelector, Action<IServiceCollection>? serviceSetup = null)
        {
            this.qualityRatingProvider = qualityRatingProvider;
            this.audioDecoder = audioDecoder;
            this.ratingSelector = ratingSelector;
            this.serviceSetup = serviceSetup;
            audioData = audioDataProvider.GetDataLazy();

            IGrader grader = ActivatorUtilities.CreateInstance<TModule>(BuildServiceProvider(new TModuleConfig()));
            if (grader is null)
                throw new ArgumentException("The service collection does not satisfy all the grader's dependencies.", nameof(serviceSetup));

            scaledConstraints = Constraints.Select(x => new FitterConstraint<TModuleConfig>(x.Inputs, x.LeftSide, x.ComparisonOperator, x.RightSide * ScalingFactor)).ToArray();
        }

        double MinimizationFunction(double[] parameters)
        {
            TModuleConfig config = BuildConfig(parameters);
            IServiceProvider serviceProvider = BuildServiceProvider(config);

            HashSet<string> existingFiles = new HashSet<string>();
            foreach (var pair in audioData)
            {
                using var stream = pair.Value();
                if (stream != null)
                    existingFiles.Add(pair.Key);
            }

            var sum = existingFiles
                .AsParallel()
                .WithDegreeOfParallelism(Environment.ProcessorCount)
                .WithExecutionMode(ParallelExecutionMode.ForceParallelism)
                .Select(x => MinimizationIteration(x, serviceProvider).Result)
                .Sum();

            return sum / existingFiles.Count;
        }

        async Task<double> MinimizationIteration(string audioId, IServiceProvider serviceProvider)
        {
            var qualityRating = qualityRatingProvider.GetRating(audioId);
            if (!qualityRating.HasValue)
                return 0d;
            double actualRating = ratingSelector(qualityRating.Value);

            using Stream? audioStream = audioData[audioId]();
            if (audioStream is null)
                return (1 + actualRating) * 100;

            if (!audioStream.CanSeek || !await audioDecoder.Supports(audioStream).ConfigureAwait(false))
                return (1 + actualRating) * 100;

            audioStream.Position = 0;
            var signals = await audioDecoder.ToChannelSignals(audioStream).ConfigureAwait(false);

            if (signals is null)
                return (1 + actualRating) * 100;

            GraderResult? grade = null;
            try
            {
                // The minimizer occassionaly tries to values out of the constrants' range, risking an exception
                grade = await ActivatorUtilities.CreateInstance<TModule>(serviceProvider).Grade(signals).ConfigureAwait(false);
            }
            catch { }

            if (grade is null)
                return (1 + actualRating) * 100;

            return Math.Abs(grade.Score - actualRating);
        }

        TModuleConfig BuildConfig(double[] parameterValues)
        {
            TModuleConfig config = new TModuleConfig();
            for (int i = 0; i < Parameters.Count; i++)
                Parameters[i].SetValue(config, parameterValues[i] / ScalingFactor);
            return config;
        }

        IServiceProvider BuildServiceProvider(TModuleConfig config)
        {
            IServiceCollection services = new DefaultServiceProviderFactory().CreateBuilder(new ServiceCollection());
            serviceSetup?.Invoke(services);
            var providerMock = new Mock<IModuleConfigurationProvider>();
            providerMock.Setup(x => x.GetConfiguration<TModuleConfig>()).Returns(config);
            services.AddSingleton(providerMock.Object);

            return services.BuildServiceProvider();
        }

        public TModuleConfig Optimize()
        {
            TModuleConfig config = new TModuleConfig();
            double[] initialParameters = Parameters.Select(x => x.GetValue(config) * ScalingFactor).ToArray();

            var constraints = scaledConstraints.Select(x => x.Create(Parameters));
            var minimizer = new Cobyla(new NonlinearObjectiveFunction(initialParameters.Length, MinimizationFunction), scaledConstraints.Select(x => x.Create(Parameters)));
            minimizer.Minimize(initialParameters);

            double error = minimizer.Value;
            double[] optimizedParameters = minimizer.Solution;

            return BuildConfig(optimizedParameters);
        }

        public double GetMeanError<T>(T? config = null) where T : class, TModuleConfig, new()
        {
            config ??= new T();
            double[] initialParameters = Parameters.Select(x => x.GetValue(config) * ScalingFactor).ToArray();
            return MinimizationFunction(initialParameters);
        }
    }
}
