﻿using Accord.Math.Optimization;
using AudioGrader.Common.Interfaces;
using AudioGrader.Graders.Noise.AverageSNRFromVAD;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimization.Fitters
{
    class AverageSnrFromVadFitter : ModuleFitter<AverageSNRFromVAD, Settings>
    {
        protected override IList<FitterParameter<Settings>> Parameters => FitterParameter<Settings>.CreateMany(
            x => x.BadThreshold,
            x => x.ExcellentThreshold);

        protected override IEnumerable<FitterConstraint<Settings>> Constraints => new[]
        {
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.BadThreshold), x => x, ConstraintType.GreaterThanOrEqualTo, 5),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.BadThreshold), x => x, ConstraintType.LesserThanOrEqualTo, 30),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.ExcellentThreshold), x => x, ConstraintType.GreaterThanOrEqualTo, 5),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.ExcellentThreshold), x => x, ConstraintType.LesserThanOrEqualTo, 70),
            new FitterConstraint<Settings>(FitterParameter<Settings>.CreateMany(x => x.BadThreshold, x => x.ExcellentThreshold), x => x[1] - x[0], ConstraintType.GreaterThanOrEqualTo, 0.01)
        };

        protected override double ScalingFactor => 0.01;

        public AverageSnrFromVadFitter(IQualityRatingProvider qualityRatingProvider, ILazyAudioDataProvider audioDataProvider, IAudioDecoder audioDecoder,
            Action<IServiceCollection>? serviceSetup = null)
            : base(qualityRatingProvider, audioDataProvider, audioDecoder, x => x.noise, serviceSetup) { }
    }
}
