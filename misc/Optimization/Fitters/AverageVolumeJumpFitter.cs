﻿using Accord.Math.Optimization;
using AudioGrader.Common.Interfaces;
using AudioGrader.Graders.Volume.AverageVolumeJump;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimization.Fitters
{
    class AverageVolumeJumpFitter : ModuleFitter<AverageVolumeJump, Settings>
    {
        protected override IList<FitterParameter<Settings>> Parameters => FitterParameter<Settings>.CreateMany(
            x => x.BadThreshold,
            x => x.ExcellentThreshold,
            x => x.AveragingInterval);

        protected override IEnumerable<FitterConstraint<Settings>> Constraints => new[]
        {
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.BadThreshold), x => x, ConstraintType.GreaterThanOrEqualTo, 0.1),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.BadThreshold), x => x, ConstraintType.LesserThanOrEqualTo, 1),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.ExcellentThreshold), x => x, ConstraintType.GreaterThanOrEqualTo, 0.5),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.ExcellentThreshold), x => x, ConstraintType.LesserThanOrEqualTo, 2),
            new FitterConstraint<Settings>(FitterParameter<Settings>.CreateMany(x => x.BadThreshold, x => x.ExcellentThreshold), x => x[1] - x[0], ConstraintType.GreaterThanOrEqualTo, 0.01),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.AveragingInterval), x => x, ConstraintType.GreaterThanOrEqualTo, 15),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.AveragingInterval), x => x, ConstraintType.LesserThanOrEqualTo, 90)
        };

        protected override double ScalingFactor => 0.01;

        public AverageVolumeJumpFitter(IQualityRatingProvider qualityRatingProvider, ILazyAudioDataProvider audioDataProvider, IAudioDecoder audioDecoder,
            Action<IServiceCollection>? serviceSetup = null)
            : base(qualityRatingProvider, audioDataProvider, audioDecoder, x => x.volumeVariation, serviceSetup) { }
    }
}
