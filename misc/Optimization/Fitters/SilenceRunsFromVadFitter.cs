﻿using Accord.Math.Optimization;
using AudioGrader.Common.Interfaces;
using AudioGrader.Graders.Silence.SilenceRunsFromVAD;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Optimization.Fitters
{
    class SilenceRunsFromVadFitter : ModuleFitter<SilenceRunsFromVAD, Settings>
    {
        protected override IList<FitterParameter<Settings>> Parameters => FitterParameter<Settings>.CreateMany(
            x => x.SilenceRunThreshold,
            x => x.RunCountBadThreshold,
            x => x.RunCountExcellentThreshold,
            x => x.RunCountWeight,
            x => x.RunAverageLengthBadThreshold,
            x => x.RunAverageLengthExcellentThreshold,
            x => x.RunAverageLengthWeight,
            x => x.SilenceRatioBadThreshold,
            x => x.SilenceRatioExcellentThreshold,
            x => x.SilenceRatioWeight);

        protected override IEnumerable<FitterConstraint<Settings>> Constraints => new[]
        {
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.RunCountWeight), x => x, ConstraintType.GreaterThanOrEqualTo, 0.01d),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.RunCountWeight), x => x, ConstraintType.LesserThanOrEqualTo, 1d),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.RunAverageLengthWeight), x => x, ConstraintType.GreaterThanOrEqualTo, 0.01d),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.RunAverageLengthWeight), x => x, ConstraintType.LesserThanOrEqualTo, 1d),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.SilenceRatioWeight), x => x, ConstraintType.GreaterThanOrEqualTo, 0.01d),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.SilenceRatioWeight), x => x, ConstraintType.LesserThanOrEqualTo, 1d),
            new FitterConstraint<Settings>(FitterParameter<Settings>.CreateMany(x => x.RunCountWeight, x => x.RunAverageLengthWeight, x => x.SilenceRatioWeight),
                x => x.Sum(), ConstraintType.EqualTo, 1d)
            // TODO Add other constraints if better training data becomes available, not worth doing it now.
        };

        protected override double ScalingFactor => 0.01;


        public SilenceRunsFromVadFitter(IQualityRatingProvider qualityRatingProvider, ILazyAudioDataProvider audioDataProvider, IAudioDecoder audioDecoder,
            Action<IServiceCollection>? serviceSetup = null)
            : base(qualityRatingProvider, audioDataProvider, audioDecoder, x => x.silenceRuns, serviceSetup) { }
    }
}
