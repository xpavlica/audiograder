﻿using Accord.Math.Optimization;
using AudioGrader.Common.Interfaces;
using AudioGrader.Graders.Volume.VolumeHistogram;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimization.Fitters
{
    class VolumeHistogramFitter : ModuleFitter<VolumeHistogram, Settings>
    {
        protected override IList<FitterParameter<Settings>> Parameters => FitterParameter<Settings>.CreateMany(
            x => x.BadThreshold,
            x => x.ExcellentThreshold,
            x => x.FrameSize,
            x => x.BucketCount,
            x => x.SilenceThreshold,
            x => x.VolumeDifferenceThreshold,
            x => x.BucketSizeRatioThreshold);

        protected override IEnumerable<FitterConstraint<Settings>> Constraints => new[]
        {
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.BadThreshold), x => x, ConstraintType.GreaterThanOrEqualTo, 10),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.BadThreshold), x => x, ConstraintType.LesserThanOrEqualTo, 50),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.ExcellentThreshold), x => x, ConstraintType.GreaterThanOrEqualTo, 1),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.ExcellentThreshold), x => x, ConstraintType.LesserThanOrEqualTo, 25),
            new FitterConstraint<Settings>(FitterParameter<Settings>.CreateMany(x => x.BadThreshold, x => x.ExcellentThreshold), x => x[0] - x[1], ConstraintType.GreaterThanOrEqualTo, 0.01),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.FrameSize), x => x, ConstraintType.GreaterThanOrEqualTo, 0.1),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.FrameSize), x => x, ConstraintType.LesserThanOrEqualTo, 3),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.BucketCount), x => x, ConstraintType.GreaterThanOrEqualTo, 5),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.BucketCount), x => x, ConstraintType.LesserThanOrEqualTo, 20),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.SilenceThreshold), x => x, ConstraintType.GreaterThanOrEqualTo, -100),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.SilenceThreshold), x => x, ConstraintType.LesserThanOrEqualTo, -50),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.VolumeDifferenceThreshold), x => x, ConstraintType.GreaterThanOrEqualTo, 1.5),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.VolumeDifferenceThreshold), x => x, ConstraintType.LesserThanOrEqualTo, 10),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.BucketSizeRatioThreshold), x => x, ConstraintType.GreaterThanOrEqualTo, 0.1),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.BucketSizeRatioThreshold), x => x, ConstraintType.LesserThanOrEqualTo, 2)
        };

        protected override double ScalingFactor => 0.01;

        public VolumeHistogramFitter(IQualityRatingProvider qualityRatingProvider, ILazyAudioDataProvider audioDataProvider, IAudioDecoder audioDecoder,
            Action<IServiceCollection>? serviceSetup = null)
            : base(qualityRatingProvider, audioDataProvider, audioDecoder, x => x.volumeVariation, serviceSetup) { }
    }
}
