﻿using Accord.Math.Optimization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Optimization
{
    public class FitterConstraint<T>
    {
        public IReadOnlyList<FitterParameter<T>> Inputs { get; }
        public Func<double[], double> LeftSide { get; }
        public ConstraintType ComparisonOperator { get; }
        public double RightSide { get; }

        public FitterConstraint(FitterParameter<T> input, Func<double, double> leftSide, ConstraintType comparisonOperator, double rightSide)
            : this(new[] { input }, x => leftSide(x[0]), comparisonOperator, rightSide) { }

        /// <summary>
        /// Creates a new Fitter constraint
        /// </summary>
        /// <param name="inputs">The input parameters</param>
        /// <param name="leftSide">The left side of the equation. The parameters are in the same order as in <paramref name="inputs"/>.</param>
        /// <param name="comparisonOperator">The comparison operator</param>
        /// <param name="rightSide">The value on the right side of the equation</param>
        public FitterConstraint(IReadOnlyList<FitterParameter<T>> inputs, Func<double[], double> leftSide, ConstraintType comparisonOperator, double rightSide)
        {
            Inputs = inputs;
            LeftSide = leftSide;
            ComparisonOperator = comparisonOperator;
            RightSide = rightSide;
        }


        public NonlinearConstraint Create(IList<FitterParameter<T>> parameterOrder)
        {
            int[] indexes = Inputs.Select(x => parameterOrder.IndexOf(x, FitterParameterEqualityComparer<T>.Default)).ToArray();
            if (indexes.Contains(-1))
                throw new ArgumentException("Not all given properties are in the parameter list.", nameof(parameterOrder));

            return new NonlinearConstraint(parameterOrder.Count, allParameters => LeftSide(indexes.Select(x => allParameters[x]).ToArray()), ComparisonOperator, RightSide);
        }
    }
}
