﻿using Accord.Math.Optimization;
using AudioGrader.Common.Interfaces;
using AudioGrader.Graders.Cutoff.VoiceCutoffByRms;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimization.Fitters
{
    class VoiceCutoffByRmsFitter : ModuleFitter<VoiceCutoffByRms, Settings>
    {
        protected override IList<FitterParameter<Settings>> Parameters => FitterParameter<Settings>.CreateMany(
           x => x.BadRatio,
           x => x.FrameSize,
           x => x.ExcellentRatio);

        protected override IEnumerable<FitterConstraint<Settings>> Constraints => new[]
        {
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.BadRatio), x => x, ConstraintType.GreaterThanOrEqualTo, 0.01),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.BadRatio), x => x, ConstraintType.LesserThanOrEqualTo, 1.5d),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.ExcellentRatio), x => x, ConstraintType.GreaterThanOrEqualTo, 0.001d),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.ExcellentRatio), x => x, ConstraintType.LesserThanOrEqualTo, 0.5d),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.FrameSize), x => x, ConstraintType.GreaterThanOrEqualTo, 0.01d),
            new FitterConstraint<Settings>(new FitterParameter<Settings>(x => x.FrameSize), x => x, ConstraintType.LesserThanOrEqualTo, 3d),
            new FitterConstraint<Settings>(FitterParameter<Settings>.CreateMany(x => x.BadRatio, x => x.ExcellentRatio), x => x[0] - x[1], ConstraintType.GreaterThanOrEqualTo, 0.01)
        };

        protected override double ScalingFactor => 0.01;


        public VoiceCutoffByRmsFitter(IQualityRatingProvider qualityRatingProvider, ILazyAudioDataProvider audioDataProvider, IAudioDecoder audioDecoder,
            Action<IServiceCollection>? serviceSetup = null)
            : base(qualityRatingProvider, audioDataProvider, audioDecoder, x => x.suddenEnd, serviceSetup) { }
    }
}
