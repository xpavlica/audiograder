﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Optimization
{
    public interface ILazyAudioDataProvider
    {
        /// <summary>
        /// Returns record id - audio data stream pairings.
        /// </summary>
        /// <returns></returns>
        IDictionary<string, Func<Stream?>> GetDataLazy();
    }
}
