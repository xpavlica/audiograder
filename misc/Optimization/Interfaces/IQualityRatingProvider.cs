﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimization
{
    public interface IQualityRatingProvider
    {
        /// <summary>
        /// Gets the manual rating data for the record with id <paramref name="audioId"/>.
        /// </summary>
        /// <param name="audioId">The audio id</param>
        /// <returns>The <see cref="QualityRating"/> of the record, if found. Null otherwise./></returns>
        QualityRating? GetRating(string audioId);
    }
}
