﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimization
{
    public struct QualityRating
    {
        public float overall;
        public float noise;
        public float suddenEnd;
        public float clipping;
        public float volumeVariation;
        public float silenceRuns;

        public override bool Equals(object? obj)
        {
            if (!(obj is QualityRating other))
                return false;
            return overall == other.overall && 
                noise == other.noise && 
                suddenEnd == other.suddenEnd && 
                clipping == other.clipping && 
                volumeVariation == other.volumeVariation &&
                silenceRuns == other.silenceRuns;
        }

        public override int GetHashCode()
        {
            return (int)(overall + noise * 10f + suddenEnd * 100f + clipping * 1000f + volumeVariation * 10000f + silenceRuns * 100000f);
        }

        public static bool operator ==(QualityRating left, QualityRating right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(QualityRating left, QualityRating right)
        {
            return !(left == right);
        }
    }
}
