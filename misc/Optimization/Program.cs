﻿using AudioGrader.Common.Interfaces;
using AudioGrader.Decoders.FFmpegDecoder;
using AudioGrader.Graders.Clipping.ClippingHistogram;
using AudioGrader.Graders.Cutoff.VoiceCutoffByRms;
using AudioGrader.Graders.Noise.AverageSNRFromVAD;
using AudioGrader.Graders.Silence.SilenceRunsFromVAD;
using AudioGrader.Graders.Volume.AverageVolumeJump;
using AudioGrader.Graders.Volume.VolumeHistogram;
using AudioGrader.Preprocessors.RingingToneCutter;
using Castle.DynamicProxy.Contributors;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Moq;
using Newtonsoft.Json;
using Optimization.Fitters;
using Optimization.Providers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace Optimization
{
    class Program
    {
        static readonly string ExecutableDirectory = Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().GetName().CodeBase ?? ".")?.LocalPath) ?? ".";
        static readonly string TelesalesCsvPath = Path.Combine(ExecutableDirectory, "Data", "telesales_data.csv");
        static readonly string SupportCsvPath = Path.Combine(ExecutableDirectory, "Data", "support_data.csv");
        static readonly string AudioDataPath = Path.Combine(ExecutableDirectory, "Data", "Recordings");

        static readonly ConfigurationProvider ConfigurationProvider = new ConfigurationProvider();

        static IQualityRatingProvider SupportRatingProvider => new SupportCsvQualityRatingProvider(SupportCsvPath);
        static ILazyAudioDataProvider SupportAudioDataProvider => new CsvAudioDataProvider(AudioDataPath, SupportCsvPath, 6, "wav", TelesalesCsvQualityRatingProvider.ConfigurationAction);
        static IAudioDecoder AudioDecoder => new FFmpegDecoder();
        static Action<IServiceCollection> GradingModuleServiceSetup => x => x.AddSingleton(Mock.Of<Serilog.ILogger>());

        static readonly Type[] Graders = new[]
        {
             typeof(ClippingHistogram),
             typeof(VoiceCutoffByRms),
             typeof(AverageSNRFromVAD),
             typeof(SilenceRunsFromVAD),
             typeof(AverageVolumeJump),
             typeof(VolumeHistogram)
        };

        static readonly Func<object>[] FitterTasks = new Func<object>[]
        {
            OptimizeClipping,
            OptimizeNoise,
            OptimizeVolumeJump,
            OptimizeVolumeHistogram,
            OptimizeFinalWeights
        };

        static void Main(string[] args)
        {
            Console.WriteLine("Starting...");
            foreach (var task in FitterTasks)
            {
                Console.WriteLine($"Running {task.Method.Name}");
                try
                {
                    var config = task();
                    if (config != null)
                        ConfigurationProvider.Overrides.Add(config.GetType(), config);
                    Console.WriteLine($"Finished running task {task.Method.Name}. The result is:");
                    Console.WriteLine(JsonConvert.SerializeObject(config));
                }
                catch (Exception ex)
                {
                    ex = ex.GetBaseException();
                    Console.Error.WriteLine($"An exception occured while executing the task {task.Method.Name}-.");
                    Console.Error.WriteLine(ex.Message);
                    Console.Error.WriteLine(ex.StackTrace);
                }
            }
            Console.WriteLine("Finished successfully.");
        }

        static IDictionary<Type, double> OptimizeFinalWeights()
        {
            IServiceProvider serviceProvider = new DefaultServiceProviderFactory().CreateServiceProvider(
                new ServiceCollection()
                .AddSingleton(Mock.Of<Serilog.ILogger>())
                .AddSingleton<IModuleConfigurationProvider>(new ConfigurationProvider()));

            var weightFitter = new GradingWeightFitter(
                new TelesalesCsvQualityRatingProvider(TelesalesCsvPath),
                new CsvAudioDataProvider(AudioDataPath, TelesalesCsvPath, 3, "wav", TelesalesCsvQualityRatingProvider.ConfigurationAction),
                serviceProvider,
                Graders,
                new FFmpegDecoder());

            var optimizedWeights = Task.Run(async () => await weightFitter.Optimize().ConfigureAwait(false)).Result;
            return optimizedWeights;
        }

        static AudioGrader.Graders.Clipping.ClippingHistogram.Settings OptimizeClipping()
        {
            var clippingFitter = new ClippingHistogramFitter(SupportRatingProvider, SupportAudioDataProvider, AudioDecoder, GradingModuleServiceSetup);
            return clippingFitter.Optimize();
        }

        static AudioGrader.Graders.Noise.AverageSNRFromVAD.Settings OptimizeNoise()
        {
            var noiseFitter = new AverageSnrFromVadFitter(SupportRatingProvider, SupportAudioDataProvider, AudioDecoder, GradingModuleServiceSetup);
            return noiseFitter.Optimize();
        }

        static AudioGrader.Graders.Volume.AverageVolumeJump.Settings OptimizeVolumeJump()
        {
            var jumpFitter = new AverageVolumeJumpFitter(SupportRatingProvider, SupportAudioDataProvider, AudioDecoder, GradingModuleServiceSetup);
            return jumpFitter.Optimize();
        }

        static AudioGrader.Graders.Volume.VolumeHistogram.Settings OptimizeVolumeHistogram()
        {
            var histogramFitter = new VolumeHistogramFitter(SupportRatingProvider, SupportAudioDataProvider, AudioDecoder, GradingModuleServiceSetup);
            return histogramFitter.Optimize();
        }
    }
}
