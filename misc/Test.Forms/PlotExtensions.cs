﻿using ScottPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test.Forms
{
    public static class PlotExtensions
    {
        public static void PlotSignal<T>(this Plot plot, IEnumerable<T> samples) where T : IConvertible
        {
            plot.PlotSignal(samples.Select(x => x.ToDouble(System.Globalization.CultureInfo.InvariantCulture)).ToArray());
        }
    }
}
