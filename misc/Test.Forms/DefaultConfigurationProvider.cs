﻿using AudioGrader.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test.Forms
{
    class DefaultConfigurationProvider : IModuleConfigurationProvider
    {
        public TConfigModel GetConfiguration<TConfigModel>() where TConfigModel : class, IModuleConfiguration<TConfigModel>, new()
        {
            return new TConfigModel();
        }
    }
}
