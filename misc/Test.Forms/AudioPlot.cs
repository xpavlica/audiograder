﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AudioGrader.Common.Interfaces;
using AudioGrader.Common.Models;
using AudioGrader.Decoders.FFmpegDecoder;
using AudioGrader.Decoders.WavDecoder;
using AudioGrader.Graders.Noise.AverageSNRFromVAD;
using AudioGrader.Graders.Silence.SilenceRunsFromVAD;
using AudioGrader.Preprocessors.RingingToneCutter;
using AudioGrader.Utilities.NWavesExtensions;
using AudioGrader.Utilities.VoiceActivityDetector;
using LinqStatistics;
using Moq;
using NWaves.Signals;
using NWaves.Transforms;
using ScottPlot;

namespace Test.Forms
{
    public partial class AudioPlot : Form
    {
        IEnumerable<DiscreteSignal> signals;
        readonly IAudioDecoder decoder = new FFmpegDecoder();
        static readonly IModuleConfigurationProvider ConfigurationProvider = new DefaultConfigurationProvider();

        public AudioPlot()
        {
            InitializeComponent();
            plotOriginal.plt.Style(Color.White, Color.White, ColorTranslator.FromHtml("#efefef"), Color.Black, Color.Black, Color.Black);
            plotFilter.plt.Style(Color.White, Color.White, ColorTranslator.FromHtml("#efefef"), Color.Black, Color.Black, Color.Black);
        }

        private async void btnLoad_Click(object sender, EventArgs e)
        {
            FileDialog fileDialog = new OpenFileDialog();

            if (fileDialog.ShowDialog() != DialogResult.OK)
                return;

            using (Stream file = File.OpenRead(fileDialog.FileName))
            {
                if (!await decoder.Supports(file))
                {
                    MessageBox.Show("Unsupported format.");
                    return;
                }

                file.Position = 0;
                signals = (await decoder.ToChannelSignals(file)).Select(x => x.ToNWavesDiscreteSignal());
            }

            plotOriginal.plt.Clear();
            plotOriginal.plt.PlotSignal(signals.First().Samples.Select(x => (double)x).ToArray());
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            plotFilter.plt.Clear();

            var grader = new SilenceRunsFromVAD(ConfigurationProvider);
            grader.Grade(signals.Select(x => x.ToDto())).Wait();
            PlotVoiceActivityDetection();
        }

        protected void PlotMagnitudeFft()
        {
            var fft = new Fft(1024);

            var magnitudeSpectrum = fft.MagnitudeSpectrum(signals.DownmixToMono());
            plotFilter.plt.PlotSignal(magnitudeSpectrum.Samples);
        }

        protected void PlotRingtoneCutter()
        {
            var providerMock = new Mock<IModuleConfigurationProvider>();
            providerMock.Setup(x => x.GetConfiguration<AudioGrader.Preprocessors.RingingToneCutter.Settings>()).Returns(new AudioGrader.Preprocessors.RingingToneCutter.Settings()
            {
                FrameSize = 0.05f
            });
            RingingToneCutter cutter = new RingingToneCutter(providerMock.Object);
            var cutSignal = cutter.Apply(signals.DownmixToMono().ToDto()).Result;
            plotFilter.plt.PlotSignal(cutSignal.Samples);
        }

        protected void PlotVoiceActivityDetection()
        {
            BaseThresholds baseThresholds = BaseThresholds.Default;
            baseThresholds.energy = 480;
            baseThresholds.dominantFrequencyComponent = 180;
            baseThresholds.spectralFlatness = 2.7f;
            var voice = VoiceActivityDetector.Detect(signals.DownmixToMono(), baseThresholds);

            plotFilter.plt.PlotSignal(voice);
        }

        protected void PlotAmplitudeHistogram()
        {
            var histogram = signals.DownmixToMono().Samples.Histogram(30, BinningMode.MaxValueInclusive);
            plotFilter.plt.PlotBar(DataGen.Consecutive(histogram.Count()), histogram.Select(x => (double)x.Count).ToArray());
        }

#if DEBUG
        protected void PlotVolumeHistogram()
        {
            var histogram = AudioGrader.Graders.Volume.VolumeHistogram.VolumeHistogram.GetHistogram(signals, new AudioGrader.Graders.Volume.VolumeHistogram.Settings());
            plotFilter.plt.PlotBar(DataGen.Consecutive(histogram.Count()), histogram.Select(x => (double)x.Count).ToArray());
        }
#endif
    }
}